// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   llist.cc : Linked list code : ripped from Cs161

*/
// =============================================================== 

#include "basic.hh"
#include "llist.hh"

// =============================================================== 
// ELEMENT::ELEMENT
// 	Initialize a list element, so it can be added somewhere on a list.
//
//	"item" is the item to be put on the list.  It can be a pointer
//		to anything.
// =============================================================== 

ELEMENT::ELEMENT(void *itemPtr) {
	item = itemPtr;
	next = NULL;	// assume we'll put it at the end of the list 
}

// ===============================================================
// LLIST::LLIST
//	Initialize a list, empty to start with.
//	Elements can now be added to the list.
// ===============================================================

LLIST::LLIST() {
	first = last = NULL;
	pFree = NULL;
}

// ===============================================================
// LLIST::LLIST void ((*pFreeFunc) (void * item))
//	Initialize a list, empty to start with.
//	Elements can now be added to the list.
// ===============================================================

LLIST::LLIST(void(*pFreeFunc) (void * item)) {
	first = last = NULL;
	pFree = pFreeFunc;
}

// ===============================================================
// LLIST::~LLIST
//	Prepare a list for deallocation.  If the list still contains any 
//	ELEMENTs, de-allocate them.  If possible, we also try to
//	de-allocate the "items" on the list.
// ===============================================================

LLIST::~LLIST() {
	void * item;

	while ((item = remove()) != NULL)
		if (pFree != NULL) pFree(item);	 // delete all the list elements
}

// ===============================================================
// LLIST::append
//      Append an "item" to the end of the list.
//      
//	Allocate a ELEMENT to keep track of the item.
//      If the list is empty, then this will be the only element.
//	Otherwise, put it at the end.
//
//	"item" is the thing to put on the list, it can be a pointer to 
//		anything.
// ===============================================================

void
LLIST::append(void *item) {
	ELEMENT *element = new ELEMENT(item);

	if (is_empty()) {		// list is empty
		first = element;
		last = element;
	} else {			// else put it after last
		last->next = element;
		last = element;
	}
}

// ===============================================================
// LLIST::prepend
//      Put an "item" on the front of the list.
//      
//	Allocate a ELEMENT to keep track of the item.
//      If the list is empty, then this will be the only element.
//	Otherwise, put it at the beginning.
//
//	"item" is the thing to put on the list, it can be a pointer to 
//		anything.
// ===============================================================

void
LLIST::prepend(void *item) {
	ELEMENT *element = new ELEMENT(item);

	if (is_empty()) {		// list is empty
		first = element;
		last = element;
	} else {			// else put it before first
		element->next = first;
		first = element;
	}
}

// ===============================================================
// LLIST::remove
//      Remove the first "item" from the front of the list.
// 
// Returns:
//	Pointer to removed item, NULL if nothing on the list.
// ===============================================================

void *
LLIST::remove() {
	ELEMENT *element = first;
	void *thing;

	if (is_empty())
		return NULL;

	thing = first->item;

	if (first == last) {	// list had one item, now has none 
		first = NULL;
		last = NULL;
	} else {
		first = element->next;
	}

	delete element;
	return thing;
}

// ===============================================================
// LLIST::remove(void *item)
//        Remove an element from the list, given a pointer
//        to the actual item.
// ===============================================================

void *
LLIST::remove(void* item)        // Remove cell containing item from list
{
	ELEMENT** pp_elem = &first;
	ELEMENT * toDelete;
	ELEMENT * tLast = NULL;

	while ((*pp_elem) && ((*pp_elem)->item != item)) {
		tLast = *pp_elem;
		pp_elem = &((*pp_elem)->next);
	}

	if (*pp_elem) 		     // If item was found in the list
	{
		if (*pp_elem == last)        // update last if necessary
			last = tLast;

		toDelete = *pp_elem;
		*pp_elem = (*pp_elem)->next; // remove the cell by "skipping" over it
		delete toDelete;
	}

	return(*pp_elem);		     // return NULL if item not in list
}

// ===============================================================
// LLIST::mapcar
//	Apply a function to each item on the list, by walking through  
//	the list, one element at a time.
//
//	Unlike LISP, this mapcar does not return anything!
//
//	"func" is the procedure to apply to each element of the list.
// ===============================================================

void
LLIST::mapcar(void(*pFunc) (void * item)) {
	for (ELEMENT *ptr = first; ptr != NULL; ptr = ptr->next) {
		(*pFunc)(ptr->item);
	}
}

// ===============================================================
// LLIST::is_empty
//      Returns true if the list is empty (has no items).
// ===============================================================

bool
LLIST::is_empty() {
	if (first == NULL)
		return true;
	else
		return false;
}

// ===============================================================
// LLIST::sorted_insert
//      Insert an "item" into a list, so that the list elements are
//	sorted in increasing order by pCompare
//      
//	Allocate a ELEMENT to keep track of the item.
//      If the list is empty, then this will be the only element.
//	Otherwise, walk through the list, one element at a time,
//	to find where the new item should be placed.
//
//	"item" is the thing to put on the list, it can be a pointer to 
//		anything.
// ===============================================================

void
LLIST::sorted_insert(void *item, int(*pCompare)(void*, void*)) {
	ELEMENT *element = new ELEMENT(item);
	ELEMENT *ptr;		// keep track

	if (is_empty()) {	// if list is empty, put
		first = element;
		last = element;
	} else if ((*pCompare)(item, first->item) < 0) {
		// item goes on front of list
		element->next = first;
		first = element;
	} else {		// look for first elt in list bigger than item
		for (ptr = first; ptr->next != NULL; ptr = ptr->next) {
			if ((*pCompare)(item, ptr->next->item) < 0) {
				element->next = ptr->next;
				ptr->next = element;
				return;
			}
		}

		last->next = element;		// item goes at end of list
		last = element;
	}
}

// ===============================================================

void *
LLIST::locate(void *item, int(*pCompare)(void*, void*)) {
	for (ELEMENT *ptr = first; ptr != NULL; ptr = ptr->next) {
		if ((*pCompare)(item, ptr->item) == 0)
			return (ptr->item);
	}

	return NULL;
}
