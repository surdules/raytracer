/* =============================================================== */
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   parser.y : THE parser

*/
/* =============================================================== */

%{

#include "classes.hh"
#include "render.hh"
#include "viewmath.hh"

POLYGON * pCurrentPolygon = NULL;
LLIST * pVertexList = NULL;
int     dVertexCount = 0;

%}

%union {
FLOAT   tFLOAT;
int     tINT;
char*   ptCHAR;

POINT * ptPOINT;
RGB   * ptRGB;

TEXTURE   * ptTEXTURE;
COLOR     * ptCOLOR;
SPECS     * ptSPECS;
PIGMENT   * ptPIGMENT;
TRANSFORM * ptTRANSFORM;
LIGHT     * ptLIGHT;
WORLD     * ptWORLD;
LLIST     * ptLLIST;

BOX       * ptBOX;
ELLIPSOID * ptELLIPSOID;
SPHERE    * ptSPHERE;
CYLINDER  * ptCYLINDER;
TORUS     * ptTORUS;
CONE      * ptCONE;
PLANE     * ptPLANE;
POLYGON   * ptPOLYGON;

}

%token <tFLOAT>       LFLOAT
%token <tINT>         INT
%token <ptCHAR>       STRING

%token	LDEFINE LWORLD LLIGHTS LCAMERA LEYE LCOI LSIZE LSUPERSAMPLE LBACKGROUND
%token	LPOINTLIGHT LAMBIENT LOUTPUT LDEPTH
%token	LDIFFUSE LREFLECT LREFRACT LETA LOPACITY LDISTRIBUTION LSPECULARITY
%token	LCOLOR LPIGMENT LSPECS LINTENSITY
%token  LTEXMUD
%token	LTRANSLATE LROTATE LSCALE LTRANSFORM LRGB LTEXTURE
%token	LBOX LELLIPSOID LSPHERE LCYLINDER LTORUS LPLANE LPOLYGON LCONE

%token  ERR

%type  <ptPOINT>     r_point;

%type  <ptRGB>       r_rgb;
%type  <ptRGB>       r_l_rgb;
%type  <ptTEXTURE>   r_texture;
%type  <ptCOLOR>     r_color;

%type  <tFLOAT>      r_intensity;

%type  <tFLOAT>      r_ambient;
%type  <tFLOAT>      r_diffuse;

%type  <tFLOAT>      r_opacity;
%type  <tFLOAT>      r_eta;

%type  <tFLOAT>      r_reflect;
%type  <tFLOAT>      r_refract;

%type  <tFLOAT>      r_distribution;
%type  <tFLOAT>      r_specularity;

%type  <ptSPECS>     r_specs;
%type  <ptPIGMENT>   r_pigment;

%type  <ptPOINT>     r_translate;
%type  <ptPOINT>     r_rotate;
%type  <ptPOINT>     r_scale;
%type  <ptTRANSFORM> r_xform;

%type  <ptLIGHT>     r_pointlight;

%type  <ptBOX>       r_box;
%type  <ptELLIPSOID> r_ellipsoid;
%type  <ptSPHERE>    r_sphere;
%type  <ptCYLINDER>  r_cylinder;
%type  <ptTORUS>     r_torus;
%type  <ptPLANE>     r_plane;
%type  <ptPOLYGON>   r_polygon;
%type  <ptCONE>      r_cone;

/* Grammar follows : */

%%

/* ========================================================== */
/* top-level section : all non-terminals begin with 't_' here */

input:	        t_definitions t_world t_lights t_objects
		;

t_definitions:	/* empty line */
		| t_definitions LDEFINE STRING '=' r_pigment
		;

t_world:	/* empty line */
		| LWORLD '{' r_world '}'
		;

t_lights:	/* empty line */
		| LLIGHTS '{' r_lights '}' 
		;

t_objects:      /* empty line */
		| t_objects r_box
		| t_objects r_ellipsoid
		| t_objects r_sphere
		| t_objects r_cylinder
		| t_objects r_torus
		| t_objects r_plane
		| t_objects r_polygon
		| t_objects r_cone
                ;


/* ========================================================== */
/* rules section */

r_point:	'<' LFLOAT ',' LFLOAT ',' LFLOAT '>'
                  { $$ = new POINT($2, $4, $6); }
		;

r_mpoint:       /* empty line */
                | r_mpoint r_point
                  { assert (pVertexList != NULL);
                    pVertexList->prepend((void*) $2);
		    dVertexCount++;
                  }
                ;

r_rgb:		'<' LFLOAT ',' LFLOAT ',' LFLOAT '>'
		  { $$ = new RGB($2, $4, $6); }
		;

r_texture:      '"' STRING '"'
		  { $$ = new TEXTURE ($2); }
                | LTEXMUD
                  { $$ = new TEXTURE();
		    $$->cType = cFUNCTION;
		    $$->uContent.pProcedure = tex_mud; }
		;

/* ================================================================== */
/* Pigment rules : */

r_ambient :     /* empty line */
			{ $$ = 0.3; }
		| LAMBIENT '<' LFLOAT '>'
		        { $$ = $3; }
		;


r_diffuse :     /* empty line */
			{ $$ = 0.5; }
		| LDIFFUSE '<' LFLOAT '>'
		        { $$ = $3; }
		;

r_reflect :     /* empty line */
			{ $$ = 0.0; }
		| LREFLECT '<' LFLOAT '>'
		        { $$ = $3; }
		;


r_refract :     /* empty line */
			{ $$ = 0.0; }
		| LREFRACT '<' LFLOAT '>'
		        { $$ = $3; }
		;

r_opacity:      /* empty line */
			{ $$ = 1.0; }
		| LOPACITY '<' LFLOAT '>'
		        { $$ = $3; }
		;

r_eta:          /* empty line */
			{ $$ = 0.0; }
		| LETA '<' LFLOAT '>'
		        { $$ = $3; }
		;

r_distribution: /* empty line */
			{ $$ = -1.0; }
		| LDISTRIBUTION '<' LFLOAT '>'
		        { $$ = $3; }
		;

r_specularity:  /* empty line */
			{ $$ = 0.0; }
		| LSPECULARITY '<' LFLOAT '>'
		        { $$ = $3; }
		;

r_specs:        /* empty line */
			{ $$ = new SPECS(); }
		| LSPECS '{'  r_ambient r_diffuse
                              r_reflect r_refract
                              r_eta r_opacity
			      r_distribution r_specularity '}'
			{ $$ = new SPECS($3, $4, $5, $6, $7, $8, $9, $10); }
		;


r_color:        LRGB r_rgb
                        { $$ = new COLOR ($2); } 
			
	        | LTEXTURE r_texture 
                        { $$ = new COLOR ($2); } 
		;

r_pigment:	/* empty line */
			{ $$ = new PIGMENT(0.0, 0.0, 0.0); }

		| LPIGMENT '{' r_color r_specs '}'
		        { $$ = new PIGMENT ($3, $4); } 
		;

/* ================================================================== */
/* Xform rules : */

r_translate:    /* empty line */
		    { $$ = new POINT(); }
		| LTRANSLATE r_point
		    { $$ = $2; }
		;

r_rotate:       /* empty line */
		    { $$ = new POINT(); }
		| LROTATE r_point
		    { $$ = $2; }
		;

r_scale:        /* empty line */
		    { $$ = new POINT(1.0, 1.0, 1.0); }
		| LSCALE r_point
		    { $$ = $2; }
		;

r_xform:        /* empty line */
		    { $$ = NULL; }
	        | LTRANSFORM '{' r_translate r_rotate r_scale '}'
		    { $$ = new TRANSFORM ($3->fX, $3->fY, $3->fZ, 
					  $4->fX, $4->fY, $4->fZ, 
					  $5->fX, $5->fY, $5->fZ ); 
		      delete $3; 
		      delete $4; 
		      delete $5; 
                    }
		;

/* ================================================================== */
/* rules for t_generic : */

r_world:	/* empty line */
		| r_world r_camera
		| r_world r_size
                | r_world r_output
		| r_world r_super
                | r_world r_depth
		| r_world r_background
		| r_world r_wambient
                ;

r_camera:	LCAMERA '{' LEYE r_point LCOI r_point '}'
		  { pWorld->pEye = $4;
		    pWorld->pCoi = $6;
		    
		    pWorld->pViewTransform = 
		      view_transform(pWorld->pEye,
				     pWorld->pCoi);
	          }
		  
		| LCAMERA '{' LEYE r_point '}'
		  { pWorld->pEye = $4;
		    pWorld->pCoi = new POINT(0.0, 0.0, 0.0);

		    pWorld->pViewTransform = 
		      view_transform(pWorld->pEye,
				     pWorld->pCoi);
	          }
		;

r_size:	        LSIZE '<' INT ',' INT '>' 
		  { pWorld->dWidth = $3;
		    pWorld->dHeight = $5;
	          }
		;

r_output:       LOUTPUT '"' STRING '"'
                  { pWorld->pOutputFile = new FILEOUT ($3); 
		    delete [] $3;
	          }
                ;

r_super:	LSUPERSAMPLE '<' INT '>' 
		  { pWorld->dSuperSample = $3; }
		;

r_depth:	LDEPTH '<' INT '>' 
		  { pWorld->dDepth = $3; }
		;

r_background:	LBACKGROUND r_pigment
		  { pWorld->pBackground = $2; }
		;

r_wambient:	LAMBIENT '<' LFLOAT '>'
		  { pWorld->fAmbient = $3; }
		;

/* ================================================================== */
/* rules for t_lights : */

r_intensity:    /* empty line */
                    { $$ = 0.0; }
                | LINTENSITY '<' LFLOAT '>'
                    { $$ = $3; }

r_lights:	/* empty line */
		| r_lights r_pointlight
		;

r_l_rgb:        /* empty line */
                    { $$ = new RGB(1.0, 1.0, 1.0); }
                | LRGB r_rgb
                    { $$ = $2; }
                ;

r_pointlight:	LPOINTLIGHT '{' r_point 
                                r_intensity
                                r_l_rgb '}' 
                  { $$ = new LIGHT($3, $4, $5); }
		;

/* ========================================================== */
/* rules for objects : */

r_box:          LBOX '{' r_point r_point r_pigment r_xform '}'
		  { $$ = new BOX($3, $4, $5, $6, true); }
		;

r_ellipsoid:    LELLIPSOID '{' r_point r_point r_pigment r_xform '}'
		  { $$ = new ELLIPSOID($3, $4->fX, $4->fY, $4->fZ, $5, $6, true);
		    delete $4;
	          }
      		;

r_sphere:       LSPHERE '{' r_point '<' LFLOAT '>' r_pigment r_xform '}'
		  { $$ = new SPHERE($3, $5, $7, $8, true); }
		;

r_cylinder:     LCYLINDER '{' r_point '<' LFLOAT '>' '<' LFLOAT '>' 
		                 r_pigment r_xform '}'
		  { $$ = new CYLINDER($3, $5, $8, $10, $11, true); }
		;

r_torus:        LTORUS '{' r_point '<' LFLOAT '>' '<' LFLOAT '>' 
                              r_pigment r_xform '}'
		  { $$ = new TORUS($3, $5, $8, $10, $11, true); }
		;

r_cone:         LCONE '{' r_point '<' LFLOAT '>' '<' LFLOAT '>' r_pigment 
                             r_xform '}'
		  { $$ = new CONE($3, $5, $8, $10, $11, true); }
		;

r_plane:        LPLANE '{' '<' LFLOAT ',' LFLOAT ',' LFLOAT ',' LFLOAT '>' 
                              r_pigment r_xform '}'
		  { $$ = new PLANE($4, $6, $8, $10, $12, $13, true); }
		;

r_polygon:      LPOLYGON 
		  { $<ptPOLYGON>$ = new POLYGON(true); 
		    pCurrentPolygon = $<ptPOLYGON>$;

		    pVertexList = new LLIST();
		    dVertexCount = 0;
                  }

                  '{' r_point r_point r_point r_mpoint r_pigment r_xform '}'
		  
		  { dVertexCount += 3;

		    pCurrentPolygon->ppPointArray = new POINT*[dVertexCount];
		    pCurrentPolygon->dNumVertices = dVertexCount;

		    pCurrentPolygon->ppPointArray[0] = $4;
		    pCurrentPolygon->ppPointArray[1] = $5;
		    pCurrentPolygon->ppPointArray[2] = $6;

		    /* place the points in reverse linked list order in the array : */

		    dVertexCount--;
		    for (ELEMENT *ptr = pVertexList->first; 
			 ptr != NULL; 
			 ptr = ptr->next)
		      pCurrentPolygon->ppPointArray[dVertexCount--] = (POINT*) (ptr->item);

		    pCurrentPolygon->pPigment = $8;
		    pCurrentPolygon->ptOrientation = $9;

		    if (pVertexList != NULL) delete pVertexList;
		    pVertexList = NULL;
		    pCurrentPolygon = NULL;
	          }
		;
%%

/* =============================================================== */

void 
yyerror(char *s)
{
        fprintf(stderr,"Error on line %d. %s\n", yylineno, s);
        exit(1);
}
