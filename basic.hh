// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   basic.hh : Includes for basic utilities

*/
// =============================================================== 

#ifndef __BASIC_HH
#define __BASIC_HH

#include <iostream>
#include <iomanip>

#include <stdio.h>
#include <stdlib.h>

#include <assert.h>

#define _USE_MATH_DEFINES // for M_PI
#include <math.h>

#include <time.h>

using namespace std;

// replace this with 'double' if necessary
#define FLOAT double                               

const int    T_DIM = 4;           // array dimension (4x4)
const int    B_FACE = 6;           // number of faces in a box
const int    B_VERT = 4;           // number of vertices in a box face
const double EPSILON = 0.0000001;   // anything less than this is really 0.0
const double JITTER = 0.05;        // jitter ray up from intersection point
const double DEGREES_TO_RADIANS = M_PI / 180.0;  // conversion
const int    DEF_SCRDIST = 1650;        // eye/screen distance

typedef unsigned char  UCHAR;
typedef unsigned short USHORT;

void error(char *mesg1, char *mesg2);
void error(char *mesg);

#endif  // __BASIC_HH
