// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   light.cc : Light processing functions

*/
// =============================================================== 

#include "classes.hh"
#include "light.hh"
#include "render.hh"

// =============================================================== 

LIGHT::LIGHT() {
	fIntensity = 0.0;
	pSource = NULL;
	pRgb = NULL;

	pList->prepend((void*) this);
}

// =============================================================== 

LIGHT::LIGHT(POINT * source, FLOAT intens, RGB * rgb) {

#ifdef __ASSERT
	assert(source != NULL);
	assert(rgb != NULL);
#endif

	fIntensity = intens;
	pSource = source;
	pRgb = rgb;

	pList->prepend((void*) this);
}

// =============================================================== 

LIGHT::~LIGHT() {
	if (pRgb != NULL) delete pRgb;
	if (pSource != NULL) delete pSource;

	pList->remove((void*)this);
}

// =============================================================== 

void
LIGHT::post_process(TRANSFORM * pTrans) {
	(*pSource) *= (*pWorld->pViewTransform);

	if (pTrans != NULL)
		(*pSource) *= (*pTrans);
}

// =============================================================== 
// Used in 'mapcar'ing over the linked list of lights. We need
// this extra function because we can't take the address of a 
// method

void
light_post_process(void * item) {
	LIGHT * pLight = (LIGHT *)item;

	pLight->post_process();
}
