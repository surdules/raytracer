// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   light.hh : Light includes

*/
// ===============================================================

#ifndef __LIGHT_HH
#define __LIGHT_HH

#include "classes.hh"

void light_post_process(void * item);

#endif // __LIGHT_HH
