// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   fileio.hh : File includes

*/
// =============================================================== 

#ifndef __FILEIO_HH
#define __FILEIO_HH

#include "basic.hh"

// =============================================================== 
// RGB entry : the only difference between this and the RGB class
// is that this is scaled up to [0..255] and this is what's actually
// dumped down into the TARGA output.

class RGBTABLE
{
public:

    // Don't change the order of these data members : they need to
    // be written down to file the way they are
    UCHAR    ucBlue, ucGreen, ucRed;

    RGBTABLE()
      { ucRed = ucGreen = ucBlue = (UCHAR) 0; }

    RGBTABLE(UCHAR r, UCHAR g, UCHAR b)  
      { ucRed = r; ucGreen = g; ucBlue = b; }

    RGBTABLE(RGBTABLE * pRgbtable)
      {	      ucRed = pRgbtable->ucRed;
	      ucGreen = pRgbtable->ucGreen;
	      ucBlue = pRgbtable->ucBlue;
      }

    ~RGBTABLE() { }

    RGBTABLE operator * (FLOAT argFloat);    // scale color by factor
    RGBTABLE operator + (RGBTABLE argRgb);   // add 2 colors
    RGBTABLE operator - (RGBTABLE argRgb);   // substract 2 colors

#ifdef __DEBUG
    void print();
#endif

};

// =============================================================== 
// Output file class : should be straightforward ..

class FILEOUT
{
public:
    FILE *pOutFile;             // the actual open file
    char *pName;                // name of the file

    FILEOUT();
    FILEOUT(char *pcName);      // open named file
    ~FILEOUT();                 // closes the given file

    // write targa header to file
    void write_header(int dWidth, int dHeight);

    // write an image scanline to file
    void write_scanline(RGBTABLE *pWriteLine, int dWidth);

#ifdef __DEBUG
    void print();
#endif

};

#endif
