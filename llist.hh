// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   llist.hh : Linked list includes : ripped from Cs161

*/
// =============================================================== 

#ifndef __LLIST_HH
#define __LLIST_HH

// =============================================================== 
// The following class defines a "list element" -- which is
// used to keep track of one item on a list.  It is equivalent to a
// LISP cell, with a "car" ("next") pointing to the next element on the list,
// and a "cdr" ("item") pointing to the item on the list.
//
// Internal data structures kept public so that List operations can
// access them directly.

class ELEMENT {
public:
	ELEMENT(void * itemPtr);
	ELEMENT *next;		// next element on list, 
						// NULL if this is the last

	void *item; 	    // pointer to item on the list
};

// =============================================================== 
// The following class defines a "list" -- a singly linked list of
// list elements, each of which points to a single item on the list.
//
// By using the "Sorted" functions, the list can be kept in sorted
// in increasing order by "key" in ListElement.

class LLIST {
public:
	LLIST();			// initialize the list
	LLIST(void(*pFreeFunc) (void * item)); // function to free elements	
	~LLIST();			// de-allocate the list

	void prepend(void *item); 	// Put item at the beginning of the list
	void append(void *item); 	// Put item at the end of the list
	void *remove(); 	 	// Take item off the front of the list
	void *remove(void* item);   // Remove cell containing item from list

	void mapcar(void(*pFunc) (void * item));	// Apply "func" to every element 
												// on the list
	bool is_empty();		// is the list empty? 

	void sorted_insert(void *item, int(*pCompare)(void*, void*));
	void * locate(void *item, int(*pCompare)(void*, void*));

	ELEMENT *first;  	// Head of the list, NULL if list is empty
	ELEMENT *last;	// Last element of list
	void(*pFree) (void * item);
};


#endif
