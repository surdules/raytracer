// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   ray.cc :

*/
// =============================================================== 

#include "classes.hh"

// =============================================================== 

RAY::RAY() {
	pStart = pEnd = NULL;
	bInside = false;
}

// =============================================================== 

RAY::RAY(POINT* pS, POINT* pE, bool inside) {
	pStart = pS; pEnd = pE;
	bInside = inside;
}

// =============================================================== 

RAY::RAY(RAY* pR) {
	assert(pR != NULL);

	pStart = pR->pStart;
	pEnd = pR->pEnd;
	bInside = pR->bInside;
}

// =============================================================== 

RAY::~RAY() {
	if (pStart != NULL) delete pStart;
	if (pEnd != NULL) delete pEnd;
}

