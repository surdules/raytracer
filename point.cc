// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   point.cc :

*/
// =============================================================== 

#include "classes.hh"
#include "transform.hh"
#include "point.hh"
#include "render.hh"

// =============================================================== 

POINT::POINT() {
	fX = fY = fZ = 0.0;
	fW = 1.0;
}

// =============================================================== 

POINT::POINT(POINT* pPoint) {
	assert(pPoint != NULL);

	fX = pPoint->fX;
	fY = pPoint->fY;
	fZ = pPoint->fZ;
	fW = pPoint->fW;
}

// =============================================================== 

POINT::POINT(FLOAT x, FLOAT y, FLOAT z) {
	fX = x; fY = y; fZ = z; fW = 1.0;
}

// =============================================================== 

POINT::~POINT() {

}

// =============================================================== 
// compute the sum of two vectors                       

POINT
POINT::operator + (POINT argPoint) {
	POINT sum;

	sum.fX = fX + argPoint.fX;
	sum.fY = fY + argPoint.fY;
	sum.fZ = fZ + argPoint.fZ;

	return (sum);
}

// =============================================================== 
// compute the difference of two vectors                       

POINT
POINT::operator - (POINT argPoint) {
	POINT diff;

	diff.fX = fX - argPoint.fX;
	diff.fY = fY - argPoint.fY;
	diff.fZ = fZ - argPoint.fZ;

	return (diff);
}

// =============================================================== 
// scale a vector                       

POINT
POINT::operator * (FLOAT argFloat) {
	POINT scale;

	scale.fX = fX * argFloat;
	scale.fY = fY * argFloat;
	scale.fZ = fZ * argFloat;

	return (scale);
}

// =============================================================== 
// Normalize a given vector 

void
POINT::normalize() {
	FLOAT length;

	length = fX*fX + fY*fY + fZ*fZ;

	if (length > 0) {
		length = sqrt(length);
		fX /= length;
		fY /= length;
		fZ /= length;
	} else
		fX = fY = fZ = 0.0;
}

// =============================================================== 
// compute the dot product of two vectors                

FLOAT
POINT::operator * (POINT argPoint) {
	FLOAT fRes;
	POINT np1(this), np2(&argPoint);

	/* normalize'em first ... */
	np1.normalize();
	np2.normalize();

	fRes = np1.fX * np2.fX +
		np1.fY * np2.fY +
		np1.fZ * np2.fZ;

	return fRes;
}

// =============================================================== 
// compute the cross product of two vectors                

POINT
POINT::operator % (POINT argPoint) {
	POINT dummy;

	dummy.fX = (fY * argPoint.fZ - fZ * argPoint.fY);
	dummy.fY = (fZ * argPoint.fX - fX * argPoint.fZ);
	dummy.fZ = (fX * argPoint.fY - fY * argPoint.fX);

	return dummy;
}

// =============================================================== 

POINT
POINT::operator *= (TRANSFORM trans) {
	(*this) = (*this) * trans;
	return (*this);
}

// =============================================================== 
// Multiply a vector by a given matrix 

POINT
POINT::operator * (TRANSFORM trans) {
	POINT product;

	product.fX = fX * trans.faValues[0][0] + fY * trans.faValues[1][0] +
		fZ * trans.faValues[2][0] + fW * trans.faValues[3][0];

	product.fY = fX * trans.faValues[0][1] + fY * trans.faValues[1][1] +
		fZ * trans.faValues[2][1] + fW * trans.faValues[3][1];

	product.fZ = fX * trans.faValues[0][2] + fY * trans.faValues[1][2] +
		fZ * trans.faValues[2][2] + fW * trans.faValues[3][2];

	product.fW = fX * trans.faValues[0][3] + fY * trans.faValues[1][3] +
		fZ * trans.faValues[2][3] + fW * trans.faValues[3][3];

	if (fabs(product.fX) < EPSILON) product.fX = 0.0;
	if (fabs(product.fY) < EPSILON) product.fY = 0.0;
	if (fabs(product.fZ) < EPSILON) product.fZ = 0.0;
	if (fabs(product.fW) < EPSILON) product.fW = 0.0;

	return product;
}

// =============================================================== 
// compute the distance between two vectors              

FLOAT
POINT::distance(POINT* pTo) {
	FLOAT temp;

	temp = (fX - pTo->fX) * (fX - pTo->fX);
	temp += (fY - pTo->fY) * (fY - pTo->fY);
	temp += (fZ - pTo->fZ) * (fZ - pTo->fZ);

	return(sqrt(temp));
}

