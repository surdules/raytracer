// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   fileio.cc : File related functions

*/
// =============================================================== 

#include "basic.hh"
#include "fileio.hh"

// =============================================================== 

FILEOUT::FILEOUT() {
	pOutFile = NULL;
	pName = NULL;
}

// =============================================================== 
// Opens the file name : copy the given file name inside the object

FILEOUT::FILEOUT(char * pcName) {
	if (fopen_s(&pOutFile, pcName, "wb") != 0)
		error("Can't open output file", pcName);

	pName = new char[strlen(pcName) + 1];
	strcpy_s(pName, strlen(pcName) + 1, pcName);
}

// =============================================================== 

FILEOUT::~FILEOUT() {
	fclose(pOutFile);
	if (pName != NULL) delete[] pName;
}

// =============================================================== 
// Writes the targa header to the output file

void
FILEOUT::write_header(int dWidth, int dHeight) {
	fputc((char)0, pOutFile);
	fputc((char)0, pOutFile);
	fputc((char)0x2, pOutFile);
	fputc((char)0, pOutFile);

	fputc((char)0, pOutFile);
	fputc((char)0, pOutFile);
	fputc((char)0, pOutFile);
	fputc((char)0, pOutFile);

	fputc((char)0, pOutFile);
	fputc((char)0, pOutFile);
	fputc((char)0, pOutFile);
	fputc((char)0, pOutFile);

	fputc((char)(dWidth & 0xff), pOutFile);
	fputc((char)((dWidth & 0xff00) >> 8), pOutFile);

	fputc((char)(dHeight & 0xff), pOutFile);
	fputc((char)((dHeight & 0xff00) >> 8), pOutFile);

	fputc((char)0x18, pOutFile);
	fputc((char)0x20, pOutFile);
}

// =============================================================== 
// Writes one scanline into the file 

void
FILEOUT::write_scanline(RGBTABLE *pScanline, int dWidth) {
	if ((fwrite(pScanline, sizeof(RGBTABLE), dWidth, pOutFile))
		!= (unsigned)dWidth)
		error("Can't write entire scanline into output", NULL);
}

