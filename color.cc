// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   color.cc : Color functions

*/
// =============================================================== 

#include "classes.hh"
#include "color.hh"
#include "render.hh"

// =============================================================== 
// ***************************************************************
// **************** RGB SECTION **********************************
// ***************************************************************
// =============================================================== 

RGB::RGB() {
	fRed = fGreen = fBlue = 0.0;
}

// =============================================================== 

RGB::RGB(FLOAT r, FLOAT g, FLOAT b) {
	fRed = r; fGreen = g; fBlue = b;
}

// =============================================================== 

RGB::RGB(RGB * pRgb) {

#ifdef __ASSERT
	assert(pRgb != NULL);
#endif

	fRed = pRgb->fRed;
	fGreen = pRgb->fGreen;
	fBlue = pRgb->fBlue;
}

// =============================================================== 

RGB::~RGB() {}

// =============================================================== 
// ***************************************************************
// **************** SPECS SECTION ********************************
// ***************************************************************
// =============================================================== 

SPECS::SPECS() {
	fAmbient = 0.3;
	fDiffuse = 0.5;

	fReflect = 0.0;
	fRefract = 0.0;

	fEta = 0.0;
	fOpacity = 1.0;

	fDistribution = -1.0;
	fSpecularity = 0.0;

	bTransparent = false;
}

// =============================================================== 

SPECS::SPECS(FLOAT amb, FLOAT diff, FLOAT refl, FLOAT refr,
	FLOAT eta, FLOAT op,
	FLOAT dist, FLOAT spec) {
	fAmbient = amb;
	fDiffuse = diff;

	fReflect = refl;
	fRefract = refr;

	fEta = eta;
	fOpacity = op;

	fDistribution = dist;
	fSpecularity = spec;

	bTransparent = (fOpacity < 1.0 &&
		fEta > 1.0 - EPSILON);
}

// =============================================================== 

SPECS::SPECS(SPECS * pSpecs) {

#ifdef __ASSERT
	assert(pSpecs != NULL);
#endif

	fAmbient = pSpecs->fAmbient;
	fDiffuse = pSpecs->fDiffuse;

	fReflect = pSpecs->fReflect;
	fRefract = pSpecs->fRefract;

	fEta = pSpecs->fEta;
	fOpacity = pSpecs->fOpacity;

	fDistribution = pSpecs->fDistribution;
	fSpecularity = pSpecs->fSpecularity;

	bTransparent = pSpecs->bTransparent;
}

// =============================================================== 

SPECS::~SPECS() {

}

// =============================================================== 
// ***************************************************************
// **************** COLOR SECTION ********************************
// ***************************************************************
// =============================================================== 

COLOR::COLOR() {
	cType = 255;
}

// =============================================================== 

COLOR::COLOR(RGB * pRgb) {

#ifdef __ASSERT
	assert(pRgb != NULL);
#endif

	cType = cRGB;
	uContent.pRgb = pRgb;
}

// =============================================================== 

COLOR::COLOR(TEXTURE * pTexture) {

#ifdef __ASSERT
	assert(pTexture != NULL);
#endif

	cType = cTEXTURE;
	uContent.pTexture = pTexture;
}

// =============================================================== 

COLOR::COLOR(COLOR * pColor) {

#ifdef __ASSERT
	assert(pColor != NULL);
#endif

	cType = pColor->cType;
	uContent = pColor->uContent;
}

// =============================================================== 

COLOR::~COLOR() {
	if (cType == cRGB) {

#ifdef __ASSERT
		assert(uContent.pRgb != NULL);
#endif

		delete uContent.pRgb;
	}

	else
		if (cType == cTEXTURE) {

#ifdef __ASSERT
			assert(uContent.pTexture != NULL);
#endif

			delete uContent.pTexture;
		}
}

// =============================================================== 
// ***************************************************************
// **************** PIGMENT SECTION ******************************
// ***************************************************************
// =============================================================== 

PIGMENT::PIGMENT() {
	pColor = NULL;

	pSpecs = NULL;
	pList->prepend((void*) this);
}

// =============================================================== 

PIGMENT::PIGMENT(COLOR * color, SPECS * specs) {

#ifdef __ASSERT
	assert(color != NULL);
	assert(specs != NULL);
#endif

	pColor = color;

	pSpecs = specs;
	pList->prepend((void*) this);
}


// =============================================================== 

PIGMENT::PIGMENT(FLOAT fRed, FLOAT fGreen, FLOAT fBlue) {
	RGB * pRgb = new RGB(fRed, fGreen, fBlue);

	pColor = new COLOR(pRgb);

	pSpecs = new SPECS();
}

// =============================================================== 

PIGMENT::~PIGMENT() {
	if (pColor != NULL) delete pColor;
	if (pSpecs != NULL) delete pSpecs;
	pList->remove((void*)this);
}

// ===============================================================
// Scale an RGB entry by a given floating point value. Check for
// overflow

RGBTABLE
RGBTABLE::operator * (FLOAT argFloat) {
	RGBTABLE dummy;

	if (argFloat < 0.0) return dummy;

	if (argFloat > 1.0) {
		dummy.ucRed = (UCHAR)255;
		dummy.ucGreen = (UCHAR)255;
		dummy.ucBlue = (UCHAR)255;

		return dummy;
	}

	dummy.ucRed = (UCHAR)(ucRed * argFloat);
	dummy.ucGreen = (UCHAR)(ucGreen * argFloat);
	dummy.ucBlue = (UCHAR)(ucBlue * argFloat);

	return dummy;
}

// ===============================================================
// Add 2 RGB entry objects and return the result. Check for overflow

RGBTABLE
RGBTABLE::operator + (RGBTABLE argRgb) {
	RGBTABLE dummy;

	if ((int)(ucRed + argRgb.ucRed) > 255) dummy.ucRed = (UCHAR)255;
	else dummy.ucRed = (UCHAR)ucRed + argRgb.ucRed;

	if ((int)(ucGreen + argRgb.ucGreen) > 255) dummy.ucGreen = (UCHAR)255;
	else dummy.ucGreen = (UCHAR)ucGreen + argRgb.ucGreen;

	if ((int)(ucBlue + argRgb.ucBlue) > 255) dummy.ucBlue = (UCHAR)255;
	else dummy.ucBlue = (UCHAR)ucBlue + argRgb.ucBlue;

	return dummy;
}

// ===============================================================
// Substract 2 RGB entry objects. Check for underflow

RGBTABLE
RGBTABLE::operator - (RGBTABLE argRgb) {
	RGBTABLE dummy;

	if (ucRed < argRgb.ucRed) dummy.ucRed = (UCHAR)0;
	else dummy.ucRed = (UCHAR)(ucRed - argRgb.ucRed);

	if (ucGreen < argRgb.ucGreen) dummy.ucGreen = (UCHAR)0;
	else dummy.ucGreen = (UCHAR)(ucGreen - argRgb.ucGreen);

	if (ucBlue < argRgb.ucBlue) dummy.ucBlue = (UCHAR)0;
	else dummy.ucBlue = (UCHAR)(ucBlue - argRgb.ucBlue);

	return dummy;
}

// =============================================================== 
// Given an array of RGB entries, fill them all up with the color
// of the background (if possible). 

void
fill_background(RGBTABLE * pScanLine, int dWidth) {
	RGBTABLE * pRgbTable;   // current array entry
	RGB      * pRgb = NULL; // background color (if any)
	int count;

	if (pScanLine == NULL) return;

	if (pWorld->pBackground->pColor->cType == cRGB)
		pRgb = pWorld->pBackground->pColor->uContent.pRgb;

	for (count = 0; count < dWidth; count++) {
		pRgbTable = &pScanLine[count];

		if (pRgb != NULL) {
			pRgbTable->ucRed = (UCHAR)(255 * pRgb->fRed);
			pRgbTable->ucGreen = (UCHAR)(255 * pRgb->fGreen);
			pRgbTable->ucBlue = (UCHAR)(255 * pRgb->fBlue);
		} else {
			// default : fill with black
			pRgbTable->ucRed = (UCHAR)0;
			pRgbTable->ucGreen = (UCHAR)0;
			pRgbTable->ucBlue = (UCHAR)0;
		}
	}
}

// =============================================================== 
// Heart of the color computing engine : given an object, a normal
// and an intersection point, compute the color of that object
// at the intersection point, based on the value of the normal.
// The pRgbResult parameter must point to a valid memory area 
// that will be appropriately filled.
//
// The formula used for computing the color goes as follows :
//
// I = computed color
// Ia = ambient intensity
// ka, kd = ambient reflectivity, diffuse (specular) reflectivity
// n = specular distribution
// ks = specular reflectivity
// Oc = object color
// Lc = light color
// p = light intensity
//
// N = normal direction
// L = light direction (from intersection point to light source)
// H = light direction + eye direction (both from intersection point)
//  
// I = Oc * Ia * ka + 
//     Lc * p * Sum[over all lights](kd * (N . L) * O + ks * (N . H)^n)

void
color_pixel(OBJECT * pObject,
	POINT * pNormal, POINT * pIntersect,
	RGBTABLE * pRgbResult) {
	COLOR * plColor;
	SPECS * plSpecs;
	RGB   * plRgb = NULL,
		pfuncRGB;
	int   iRed, iGreen, iBlue;

#ifdef __ASSERT
	assert(pObject != NULL);
	assert(pNormal != NULL);
	assert(pIntersect != NULL);
	assert(pRgbResult != NULL);
#endif

	// for now, only works if the object has
	// * RGB pigment/color
	// * procedural texture

	plColor = pObject->pPigment->pColor;
	plSpecs = pObject->pPigment->pSpecs;

	if (plColor->cType == cRGB) {
		plRgb = plColor->uContent.pRgb;
	}

	// compute procedural color if necessary : 
	else if (plColor->cType == cTEXTURE &&
		plColor->uContent.pTexture->cType == cFUNCTION) {
		pfuncRGB = (*plColor->uContent.pTexture->uContent.pProcedure)(pIntersect);
		plRgb = &pfuncRGB;
	}

	else return;

	// computed color and accumulator used for all the lights
	RGB I, accum, emptyRGB;

	// feeler ray : used in shadow computations
	RAY feeler(new POINT(pIntersect), new POINT(), false);

	// see comments above
	FLOAT ka, kd, n, ks, ambient;

	pNormal->normalize();

	ka = plSpecs->fAmbient;
	kd = plSpecs->fDiffuse;

	n = plSpecs->fDistribution;
	ks = plSpecs->fSpecularity;

	// Diffuse and specular component : iterate over all lights

	for (ELEMENT *ptrLi = LIGHT::pList->first;
		ptrLi != NULL;
		ptrLi = ptrLi->next) {

		LIGHT * pLight = ((LIGHT*)ptrLi->item);
		FLOAT dotNL, dotNH, diffuse, specular;

		POINT lightDir = *(pLight->pSource) - (*pIntersect);
		POINT eyeDir = *(pWorld->pEye) - (*pIntersect);
		POINT halfWay = lightDir + eyeDir;

		lightDir.normalize();
		eyeDir.normalize();
		halfWay.normalize();

		// Shadow feeler ray : is the object in shadow ?
		*(feeler.pStart) = *(feeler.pStart) + lightDir * JITTER;
		*(feeler.pEnd) = *(pLight->pSource);
		if (intersect_all(&feeler, NULL, NULL, NULL))
			continue;

		// compute (N . L)
		dotNL = (lightDir * (*pNormal));

		if (dotNL > 0.0) {

			// faster way of setting accum to be empty (does a memset())
			accum = emptyRGB;

			// compute (N . H)
			dotNH = (halfWay * (*pNormal));
			diffuse = kd * dotNL;

			// don't want to raise to negative (or 0) powers
			if ((int)n <= 0) specular = 0.0;
			else specular = ks * pow(dotNH, n);

			accum.fRed = (diffuse * plRgb->fRed + specular);
			accum.fGreen = (diffuse * plRgb->fGreen + specular);
			accum.fBlue = (diffuse * plRgb->fBlue + specular);

			accum.fRed *= (pLight->fIntensity * pLight->pRgb->fRed);
			accum.fGreen *= (pLight->fIntensity * pLight->pRgb->fGreen);
			accum.fBlue *= (pLight->fIntensity * pLight->pRgb->fBlue);

			I.fRed += accum.fRed;
			I.fGreen += accum.fGreen;
			I.fBlue += accum.fBlue;
		}
	}

	// Ambient component : 

	ambient = pWorld->fAmbient * ka;

	// addup the whole shabang :
	I.fRed += ambient * plRgb->fRed;
	I.fGreen += ambient * plRgb->fGreen;
	I.fBlue += ambient * plRgb->fBlue;

	// Finalize : check for overflow

	iRed = (int)(255 * I.fRed);
	iGreen = (int)(255 * I.fGreen);
	iBlue = (int)(255 * I.fBlue);

	if (iRed > 255) iRed = 255;
	if (iGreen > 255) iGreen = 255;
	if (iBlue > 255) iBlue = 255;

	// Scale down

	pRgbResult->ucRed = (UCHAR)iRed;
	pRgbResult->ucGreen = (UCHAR)iGreen;
	pRgbResult->ucBlue = (UCHAR)iBlue;
}

// =============================================================== 
// Combine colors : used to put together the colors from local,
// reflected and refracted directions for the given object
//
// The formula used is : 
//     result = local + specularity * reflected + opacity * refracted

void
combine_colors(RGBTABLE * pRgbResult,
	RGBTABLE * pLocalColor,
	RGBTABLE * pReflectedColor,
	RGBTABLE * pTransmittedColor,
	OBJECT * pObject) {

#ifdef __ASSERT
	assert(pRgbResult != NULL);
	assert(pLocalColor != NULL);
	assert(pReflectedColor != NULL);
	assert(pTransmittedColor != NULL);
	assert(pObject != NULL);
#endif

	SPECS * plSpecs = pObject->pPigment->pSpecs;

	*pRgbResult = *pLocalColor;

	// Only add the two components if their coeffs are non-trivial
	if (plSpecs->fReflect > EPSILON)
		*pRgbResult = *pRgbResult + *pReflectedColor   * plSpecs->fReflect;

	if (plSpecs->fRefract > EPSILON)
		*pRgbResult = *pRgbResult + *pTransmittedColor * plSpecs->fRefract;
}
