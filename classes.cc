// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   objects.cc : All the main object routines : constructors,
				destructors, post_processing, intersects etc.

*/
// =============================================================== 

#include "classes.hh"
#include "viewmath.hh"
#include "render.hh"

// =============================================================== 
// ***************************************************************
// **************** BUFFER SECTION *******************************
// ***************************************************************
// =============================================================== 

BUFFER::BUFFER() {
	pcBuffer = NULL; dSizeX = 0; dSizeY = 0;
}

// =============================================================== 

BUFFER::BUFFER(BUFFER * pBuffer) {
	pcBuffer = pBuffer->pcBuffer;
	dSizeX = pBuffer->dSizeX;
	dSizeY = pBuffer->dSizeY;
}
// =============================================================== 

BUFFER::BUFFER(char * pcName) {
	pcName = NULL;
}

// =============================================================== 

BUFFER::~BUFFER() {}

// =============================================================== 
// ***************************************************************
// **************** PIGMENT SECTION ******************************
// ***************************************************************
// =============================================================== 

TEXTURE::TEXTURE() {
	cType = (char)255;
}

// =============================================================== 

TEXTURE::TEXTURE(char * pcName) {
	cType = cBUFFER;
	uContent.pBuffer = new BUFFER(pcName);
}

// =============================================================== 

TEXTURE::TEXTURE(BUFFER * pBuffer) {
	cType = cBUFFER;
	uContent.pBuffer = pBuffer;
}

// =============================================================== 

TEXTURE::TEXTURE(TEXTURE * pTexture) {
	cType = pTexture->cType;
	uContent = pTexture->uContent;
}

// =============================================================== 

TEXTURE::~TEXTURE() {

}

// =============================================================== 
//      Procedural texture land 

#define round(x)       ((int)(x + 0.5))

#define MUD_FREQ       20.0
#define MUD_DIV        35.0
#define MUD_MOD        70
#define MUD_THRESH1    15
#define MUD_THRESH2    40
#define MUD_THRESH3    55

RGB
tex_mud(POINT * pPoint) {
	FLOAT radius;
	FLOAT angle;
	FLOAT x = pPoint->fX,
		y = pPoint->fY,
		z = pPoint->fZ;
	int   grain;
	RGB   clr;

	radius = sqrt(x * x / 0.2 + z * z);
	if (fabs(z) < EPSILON)
		angle = M_PI / 2.0;
	else
		angle = atan2(x, z) + M_PI;

	radius += 51 * cos(MUD_FREQ * angle + y / MUD_DIV);
	grain = round(radius) % MUD_MOD;

	if (grain % MUD_MOD - 3 == 0 &&
		fmod(x, 25.0) < 23.0) {
		/* Light color */
		clr.fRed = .15;
		clr.fGreen = .15;
		clr.fBlue = .15;
	} else if (grain < MUD_THRESH1) {
		/* chocolate color */
		clr.fRed = 0.55;
		clr.fGreen = 0.35;
		clr.fBlue = 0.17;

	} else if (grain < MUD_THRESH2) {
		/* peru  */
		clr.fRed = 0.80;
		clr.fGreen = 0.53;
		clr.fBlue = 0.25;
	} else if (grain < MUD_THRESH3) {
		clr.fRed = 0.95;
		clr.fGreen = 0.84;
		clr.fBlue = 0.58;
	} else {
		clr.fRed = 0.87;
		clr.fGreen = 0.72;
		clr.fBlue = 0.52;
	}

	return clr;
}

// =============================================================== 
// ***************************************************************
// **************** OBJECT SECTION *******************************
// ***************************************************************
// =============================================================== 

OBJECT::OBJECT(bool bAdd) {
	if (bAdd) pList->prepend((void*) this);
}

// =============================================================== 

OBJECT::~OBJECT() {
	if (ptOrientation != NULL) delete ptOrientation;
	if (pPigment != NULL) delete pPigment;

	pList->remove((void*)this);
}

// =============================================================== 
// ***************************************************************
// **************** BOX SECTION **********************************
// ***************************************************************
// =============================================================== 

BOX::BOX(bool bAdd) : OBJECT::OBJECT(bAdd) {
	p1 = p2 = NULL;

	pPigment = NULL;
	ptOrientation = NULL;
}

// =============================================================== 

BOX::BOX(POINT * po1, POINT * po2,
	PIGMENT * pigment, TRANSFORM * orient,
	bool bAdd) : OBJECT::OBJECT(bAdd) {

#ifdef __ASSERT
	assert(po1 != NULL);
	assert(po2 != NULL);
#endif

	p1 = po1;
	p2 = po2;

	pPigment = pigment;
	ptOrientation = orient;

	compute_faces();
}

// ===============================================================

BOX::~BOX() {
	if (p1 != NULL) delete p1;
	if (p2 != NULL) delete p2;

	for (int count = 0; count < B_FACE; count++)
		if (ppFaces[count] != NULL) delete ppFaces[count];
}

// ===============================================================

void
BOX::post_process(TRANSFORM * pTrans) {
	TRANSFORM localTrans;

	*p1 *= (*pWorld->pViewTransform);
	*p2 *= (*pWorld->pViewTransform);

	// compute local transformation which is the composition
	// of the orientation and user specified (if any);

	if (ptOrientation != NULL && pTrans != NULL)
		localTrans = (*ptOrientation) * (*pTrans);
	else if (ptOrientation != NULL) localTrans = *ptOrientation;
	else if (pTrans != NULL) localTrans = *pTrans;

	if (ptOrientation != NULL) {
		*p1 *= (*ptOrientation);
		*p2 *= (*ptOrientation);
	}

	if (pTrans != NULL) {
		*p1 *= (*pTrans);
		*p2 *= (*pTrans);
	}

	// post_process all the polygon's faces with the
	// same orientation as the box itself

	for (int count = 0; count < B_FACE; count++)
		ppFaces[count]->post_process(&localTrans);

	// the bounding sphere
	pbSphere = new SPHERE(new POINT(((*p1 + *p2) * 0.5)),
		p1->distance(p2) * 0.5 + JITTER,
		NULL, NULL);
}

// ===============================================================
// Compute all the faces for the given box.
//              
//                 ----------p2         ^ y
//                /|       /|           |
//               / |      / |           |
//              /  |     /  |           |       z
//             ----------   |           |     /
//             |   |    |   |           |    /
//             |   /----|---/           |   /
//             |  /     |  /            |  /
//             | /      | /             | /
//             |/       |/              |/
//           p1----------               +------------> x
//
// The faces are denominated as F(ront), L(eft), B(ack), R(ight), 
//                              T(op), (b)O(ttom)
// as the box is viewed from your point of view.
// With these denominations, the faces are numbered as follows :
//
// 0 = O; 1 = F; 2 = L      ==> all these faces share p1
// 3 = B; 4 = T; 5 = R      ==> all these faces share p2
//
// Remember that polygons are to be specified CLOCKWISE
//
// *NOTE* : all the polygons thus created as faces for the box
//          have NULL 1). orientations and 2). pigments.
//          The appropriate orientation is applied during
//          the post processing phase and the pigment is taken
//          from the box

void
BOX::compute_faces() {
	for (int count = 0; count < B_FACE; count++) {
		ppFaces[count] = new POLYGON(B_VERT);

		for (int vert = 0; vert < B_VERT; vert++)
			if (count < B_FACE / 2)
				ppFaces[count]->ppPointArray[vert] = new POINT(p1);
			else
				ppFaces[count]->ppPointArray[vert] = new POINT(p2);
	}

	// Face 0 :
	ppFaces[0]->ppPointArray[1]->fX = p2->fX;

	ppFaces[0]->ppPointArray[2]->fX = p2->fX;
	ppFaces[0]->ppPointArray[2]->fZ = p2->fZ;

	ppFaces[0]->ppPointArray[3]->fZ = p2->fZ;

	// Face 1 :
	ppFaces[1]->ppPointArray[1]->fY = p2->fY;

	ppFaces[1]->ppPointArray[2]->fX = p2->fX;
	ppFaces[1]->ppPointArray[2]->fY = p2->fY;

	ppFaces[1]->ppPointArray[3]->fX = p2->fX;

	// Face 2 :
	ppFaces[2]->ppPointArray[1]->fZ = p2->fZ;

	ppFaces[2]->ppPointArray[2]->fY = p2->fY;
	ppFaces[2]->ppPointArray[2]->fZ = p2->fZ;

	ppFaces[2]->ppPointArray[3]->fY = p2->fY;

	// Face 3 :
	ppFaces[3]->ppPointArray[1]->fX = p1->fX;

	ppFaces[3]->ppPointArray[2]->fX = p1->fX;
	ppFaces[3]->ppPointArray[2]->fY = p1->fY;

	ppFaces[3]->ppPointArray[3]->fY = p1->fY;

	// Face 4 :
	ppFaces[4]->ppPointArray[1]->fZ = p1->fZ;

	ppFaces[4]->ppPointArray[2]->fX = p1->fX;
	ppFaces[4]->ppPointArray[2]->fZ = p1->fZ;

	ppFaces[4]->ppPointArray[3]->fX = p1->fX;

	// Face 5 :
	ppFaces[5]->ppPointArray[1]->fY = p1->fY;

	ppFaces[5]->ppPointArray[2]->fY = p1->fY;
	ppFaces[5]->ppPointArray[2]->fZ = p1->fZ;

	ppFaces[5]->ppPointArray[3]->fZ = p1->fZ;

	// now compute the normals for all the faces
	for (int count = 0; count < B_FACE; count++)
		ppFaces[count]->compute_normal();
}

// ===============================================================
// Intersect with a box : very similar to "intersect_all".
// Compute intersections with all the box's faces and then pick
// the one closest to the viewer.

bool
BOX::intersect(RAY * pRay, POINT * pIntersect, POINT * pNormal) {
	POINT     intersect, normal, scrapIntersect, scrapNormal;
	POLYGON * pFace = NULL, *pScrapFace = NULL;
	bool      result = false, first = true;
	int       icount = 0;

	// first intersect with bounding sphere :
	if (!pbSphere->intersect(pRay, pIntersect, pNormal))
		return false;

	for (int count = 0; count < B_FACE; count++) {

		pScrapFace = ppFaces[count];

		if (pScrapFace->intersect(pRay, &scrapIntersect, &scrapNormal)) {

			result = true;
			icount++;

			if (first ||
				(pWorld->pEye->distance(&scrapIntersect) <
					pWorld->pEye->distance(&intersect))) {

				intersect = scrapIntersect;
				normal = scrapNormal;
				pFace = pScrapFace;

				if (first) first = false;
			}

			// a ray can't intersect a box in more than 2 places
			if (icount == 2) break;
		}
	}

	// if we found an intersection, update the fields appropriately
	if (pFace) {
		*pIntersect = intersect;
		*pNormal = normal;
	}

	return result;
}

// =============================================================== 
// ***************************************************************
// **************** ELLIPSOID SECTION ****************************
// ***************************************************************
// =============================================================== 

ELLIPSOID::ELLIPSOID(bool bAdd) : OBJECT::OBJECT(bAdd) {
	pCenter = NULL;
	fR1 = fR2 = fR3 = 0.0;

	pPigment = NULL;
	ptOrientation = NULL;
}

// =============================================================== 

ELLIPSOID::ELLIPSOID(POINT * center, FLOAT r1, FLOAT r2, FLOAT r3,
	PIGMENT * pigment, TRANSFORM * orient,
	bool bAdd) : OBJECT::OBJECT(bAdd) {

#ifdef __ASSERT
	assert(center != NULL);
	assert(r1 > EPSILON);
	assert(r2 > EPSILON);
	assert(r3 > EPSILON);
#endif

	pCenter = center;
	fR1 = r1;
	fR2 = r2;
	fR3 = r3;

	pPigment = pigment;
	ptOrientation = orient;
}

// =============================================================== 

ELLIPSOID::~ELLIPSOID() {
	if (pCenter != NULL) delete pCenter;
}

// ===============================================================

void
ELLIPSOID::post_process(TRANSFORM * pTrans) {
	(*pCenter) *= (*pWorld->pViewTransform);

	if (ptOrientation != NULL)
		(*pCenter) *= (*ptOrientation);

	if (pTrans != NULL)
		(*pCenter) *= (*pTrans);
}

// =============================================================== 

bool
ELLIPSOID::intersect(RAY * pRay, POINT * pIntersect, POINT * pNormal) {
	pRay = NULL;
	pIntersect = NULL;
	pNormal = NULL;

	return false;
}

// =============================================================== 
// ***************************************************************
// **************** SPHERE SECTION *******************************
// ***************************************************************
// =============================================================== 

SPHERE::SPHERE(bool bAdd) : OBJECT::OBJECT(bAdd) {
	pCenter = NULL;
	fRadius = 0.0;

	pPigment = NULL;
	ptOrientation = NULL;
}

// =============================================================== 

SPHERE::SPHERE(POINT * center, FLOAT radius,
	PIGMENT * pigment, TRANSFORM * orient,
	bool bAdd) : OBJECT::OBJECT(bAdd) {

#ifdef __ASSERT
	assert(center != NULL);
	assert(radius > EPSILON);
#endif

	pCenter = center;
	fRadius = radius;

	pPigment = pigment;
	ptOrientation = orient;
}

// =============================================================== 

SPHERE::~SPHERE() {
	if (pCenter != NULL) delete pCenter;
}

// ===============================================================

void
SPHERE::post_process(TRANSFORM * pTrans) {
	(*pCenter) *= (*pWorld->pViewTransform);

	if (ptOrientation != NULL)
		(*pCenter) *= (*ptOrientation);

	if (pTrans != NULL)
		(*pCenter) *= (*pTrans);
}

// =============================================================== 
// Adapted from code written by Lori Park for CS175

bool
SPHERE::intersect(RAY * pRay, POINT * pIntersect, POINT * pNormal) {
	FLOAT x1, y1, z1;		// Ray start 
	FLOAT x2, y2, z2;		// Ray end
	FLOAT xi, yi, zi;		// Intersection point 
	FLOAT i, j, k;		        // Parameters 
	FLOAT l, m, n, rad;		// Location and radius of sphere 
	FLOAT a, b, c;		        // Coefficients of quadratic in t 
	FLOAT det;			// Determinant 
	FLOAT root1, root2;		// Roots of quadratic 

	if (fRadius < EPSILON) return false;  // Degenerate Sphere 

	// Grab data from object and ray

	x1 = pRay->pStart->fX;
	y1 = pRay->pStart->fY;
	z1 = pRay->pStart->fZ;

	x2 = pRay->pEnd->fX;
	y2 = pRay->pEnd->fY;
	z2 = pRay->pEnd->fZ;

	l = pCenter->fX;
	m = pCenter->fY;
	n = pCenter->fZ;
	rad = fRadius;

	// Calculate a 

	i = x2 - x1;
	j = y2 - y1;
	k = z2 - z1;

	a = (i * i) + (j * j) + (k * k);
	if (a < EPSILON) return false;     // Degenerate RAY 

	// Calculate b 

	b = 2 * (i * (x1 - l) + j * (y1 - m) + k * (z1 - n));

	// Calculate c 

	c = (l * l) + (m * m) + (n * n) + (x1 * x1) + (y1 * y1) + (z1 * z1)
		+ 2 * (-l * x1 - m * y1 - n * z1) - (rad * rad);

	// Calculate determinant: b^2 - 4ac 

	det = (b * b) - 4 * a * c;

	// Return false if no intersection 

	if (det < 0.0) return false;

	// Compute roots 

	root1 = (-b + sqrt(det)) / (2 * a);
	root2 = (-b - sqrt(det)) / (2 * a);

	// Consolidate roots to choose closest intersection pt.

	if (root1 < 0.0 || root2 < 0.0)
		return false;
	else if (root1 < 0.0)
		root1 = root2;
	else if (root2 > 0.0 && root2 < root1)
		root1 = root2;

	// Get intersection point 

	xi = x1 + i * root1;
	yi = y1 + j * root1;
	zi = z1 + k * root1;

	// Compute normal 

	pNormal->fX = (xi - l) / rad;
	pNormal->fY = (yi - m) / rad;
	pNormal->fZ = (zi - n) / rad;

	pIntersect->fX = xi;
	pIntersect->fY = yi;
	pIntersect->fZ = zi;

	pWorld->dIntersect++;
	return true;
}

// =============================================================== 
// ***************************************************************
// **************** CYLINDER SECTION *****************************
// ***************************************************************
// =============================================================== 

CYLINDER::CYLINDER(bool bAdd) : OBJECT::OBJECT(bAdd) {
	pCenter = NULL;
	fRadius = fHeight = 0.0;

	pPigment = NULL;
	ptOrientation = NULL;
}

// =============================================================== 

CYLINDER::CYLINDER(POINT * center, FLOAT radius, FLOAT height,
	PIGMENT * pigment, TRANSFORM * orient,
	bool bAdd) : OBJECT::OBJECT(bAdd) {

#ifdef __ASSERT
	assert(center != NULL);
	assert(radius > EPSILON);
	assert(height > EPSILON);
#endif

	pCenter = center;
	fRadius = radius;
	fHeight = height;

	pPigment = pigment;
	ptOrientation = orient;
}

// =============================================================== 

CYLINDER::~CYLINDER() {
	if (pCenter != NULL) delete pCenter;
}

// ===============================================================

void
CYLINDER::post_process(TRANSFORM * pTrans) {
	(*pCenter) *= (*pWorld->pViewTransform);

	if (ptOrientation != NULL)
		(*pCenter) *= (*ptOrientation);

	if (pTrans != NULL)
		(*pCenter) *= (*pTrans);
}

// =============================================================== 

bool
CYLINDER::intersect(RAY * pRay, POINT * pIntersect, POINT * pNormal) {
	pRay = NULL;
	pIntersect = NULL;
	pNormal = NULL;

	return false;
}

// =============================================================== 
// ***************************************************************
// **************** TORUS SECTION ********************************
// ***************************************************************
// =============================================================== 

TORUS::TORUS(bool bAdd) : OBJECT::OBJECT(bAdd) {
	pCenter = NULL;
	fSmallRadius = fBigRadius = 0.0;

	pPigment = NULL;
	ptOrientation = NULL;
}

// =============================================================== 

TORUS::TORUS(POINT * center, FLOAT sradius, FLOAT bradius,
	PIGMENT * pigment, TRANSFORM * orient,
	bool bAdd) : OBJECT::OBJECT(bAdd) {

#ifdef __ASSERT
	assert(center != NULL);
	assert(sradius > EPSILON);
	assert(bradius > EPSILON);
#endif

	pCenter = center;
	fSmallRadius = sradius;
	fBigRadius = bradius;

	pPigment = pigment;
	ptOrientation = orient;
}

// =============================================================== 

TORUS::~TORUS() {
	if (pCenter != NULL) delete pCenter;
}

// ===============================================================

void
TORUS::post_process(TRANSFORM * pTrans) {
	(*pCenter) *= (*pWorld->pViewTransform);

	if (ptOrientation != NULL)
		(*pCenter) *= (*ptOrientation);

	if (pTrans != NULL)
		(*pCenter) *= (*pTrans);
}

// =============================================================== 

bool
TORUS::intersect(RAY * pRay, POINT * pIntersect, POINT * pNormal) {
	pRay = NULL;
	pIntersect = NULL;
	pNormal = NULL;

	return false;
}

// =============================================================== 
// ***************************************************************
// **************** CONE SECTION *********************************
// ***************************************************************
// =============================================================== 

CONE::CONE(bool bAdd) : OBJECT::OBJECT(bAdd) {
	pCenter = NULL;
	fRadius = fHeight = 0.0;

	pPigment = NULL;
	ptOrientation = NULL;
}

// =============================================================== 

CONE::CONE(POINT * center, FLOAT radius, FLOAT height,
	PIGMENT * pigment, TRANSFORM * orient,
	bool bAdd) : OBJECT::OBJECT(bAdd) {

#ifdef __ASSERT
	assert(center != NULL);
	assert(radius > EPSILON);
	assert(height > EPSILON);
#endif

	pCenter = center;
	fRadius = radius;
	fHeight = height;

	pPigment = pigment;
	ptOrientation = orient;
}

// =============================================================== 

CONE::~CONE() {
	if (pCenter != NULL) delete pCenter;
}

// ===============================================================

void
CONE::post_process(TRANSFORM * pTrans) {
	(*pCenter) *= (*pWorld->pViewTransform);

	if (ptOrientation != NULL)
		(*pCenter) *= (*ptOrientation);

	if (pTrans != NULL)
		(*pCenter) *= (*pTrans);
}

// =============================================================== 

bool
CONE::intersect(RAY * pRay, POINT * pIntersect, POINT * pNormal) {
	pRay = NULL;
	pIntersect = NULL;
	pNormal = NULL;

	return false;
}

// =============================================================== 
// ***************************************************************
// **************** PLANE SECTION ********************************
// ***************************************************************
// =============================================================== 

PLANE::PLANE(bool bAdd) : OBJECT::OBJECT(bAdd) {
	fA = fB = fC = fD = 0.0;
	pNorm = NULL;

	pPigment = NULL;
	ptOrientation = NULL;
}

// =============================================================== 

PLANE::PLANE(FLOAT a, FLOAT b, FLOAT c, FLOAT d,
	PIGMENT * pigment, TRANSFORM * orient,
	bool bAdd) : OBJECT::OBJECT(bAdd) {

	fA = a; fB = b; fC = c; fD = d;

	// the normal to the plane :
	pNorm = new POINT(fA, fB, fC);
	pNorm->normalize();

	pPigment = pigment;
	ptOrientation = orient;
}

// =============================================================== 

PLANE::~PLANE() {
	if (pNorm != NULL) delete pNorm;
}

// ===============================================================
// Post processing the plane : we need to rotate both the tip
// of the normal as well as the origin, take their difference to
// recompute correctly the direction of the normal.

void
PLANE::post_process(TRANSFORM * pTrans) {
	POINT origin, tip(fA, fB, fC);

	tip *= *(pWorld->pViewTransform);
	origin *= *(pWorld->pViewTransform);

	if (ptOrientation != NULL) {
		tip *= *(ptOrientation);
		origin *= *(ptOrientation);
	}

	if (pTrans != NULL) {
		tip *= *(pTrans);
		origin *= *(pTrans);
	}

	tip = tip - origin;

	fA = tip.fX;
	fB = tip.fY;
	fC = tip.fZ;

	pNorm->fX = fA;
	pNorm->fY = fB;
	pNorm->fZ = fC;
	pNorm->normalize();
}

// =============================================================== 

bool
PLANE::intersect(RAY * pRay, POINT * pIntersect, POINT * pNormal) {
	FLOAT x1, y1, z1;		// Ray start
	FLOAT x2, y2, z2;		// Ray end 
	FLOAT xi, yi, zi;		// Intersection point 
	FLOAT i, j, k;		        // Parameters 
	FLOAT t, denom;

	// Grab data from ray 	

	x1 = pRay->pStart->fX;
	y1 = pRay->pStart->fY;
	z1 = pRay->pStart->fZ;

	x2 = pRay->pEnd->fX;
	y2 = pRay->pEnd->fY;
	z2 = pRay->pEnd->fZ;

	i = x2 - x1;
	j = y2 - y1;
	k = z2 - z1;

	// Make sure the ray isn't paralel to the plane : 

	denom = (fA * i + fB * j + fC * k);
	if (fabs(denom) < EPSILON) return false;

	t = -(fA * x1 + fB * y1 + fC * z1 + fD) / denom;

	// Return false if no intersection 

	if (t < EPSILON) return false;

	// Get intersection point 

	xi = x1 + i * t;
	yi = y1 + j * t;
	zi = z1 + k * t;

	*pNormal = *pNorm;

	pIntersect->fX = xi;
	pIntersect->fY = yi;
	pIntersect->fZ = zi;

	pWorld->dIntersect++;
	return true;
}

// =============================================================== 
// ***************************************************************
// **************** POLYGON SECTION ******************************
// ***************************************************************
// =============================================================== 

POLYGON::POLYGON(bool bAdd) : OBJECT::OBJECT(bAdd) {
	dNumVertices = 0;
	ppPointArray = NULL;
	pCopyArray = NULL;
	pPlane = NULL;

	pPigment = NULL;
	ptOrientation = NULL;
}

// =============================================================== 

POLYGON::POLYGON(int numVert, bool bAdd) : OBJECT::OBJECT(bAdd) {

#ifdef __ASSERT
	assert(numVert >= 3);
#endif

	dNumVertices = numVert;
	ppPointArray = new POINT*[dNumVertices];
	pCopyArray = NULL;
	pPlane = NULL;

	pPigment = NULL;
	ptOrientation = NULL;
}

// =============================================================== 

POLYGON::~POLYGON() {
	// free the array of vertices as well as the vertices themselves
	if (ppPointArray != NULL && dNumVertices > 0) {
		for (int count = 0; count < dNumVertices; count++)
			if (ppPointArray[count] != NULL) delete ppPointArray[count];

		delete[] ppPointArray;
	}

	// free auxiliary data 
	if (pCopyArray != NULL) delete[] pCopyArray;
	if (pPlane != NULL) delete pPlane;
}

// =============================================================== 

void
POLYGON::post_process(TRANSFORM * pTrans) {
	FLOAT rad;

	// apply transformations to all the points :
	pCenter = new POINT();
	fRadius = 0.0;

	for (int count = 0; count < dNumVertices; count++) {
		(*ppPointArray[count]) *= (*pWorld->pViewTransform);

		if (ptOrientation != NULL)
			(*ppPointArray[count]) *= (*ptOrientation);

		if (pTrans != NULL)
			(*ppPointArray[count]) *= (*pTrans);

		*pCenter = *pCenter + *ppPointArray[count];
	}

	*pCenter = *pCenter * (1.0 / dNumVertices);

	for (int count = 0; count < dNumVertices; count++) {
		if ((rad = pCenter->distance(ppPointArray[count])) > fRadius)
			fRadius = rad;
	}

	fRadius += JITTER;

	compute_normal();
}

// =============================================================== 
// Compute the normal to our polygon and store it. This function
// should be called after a polygon is allocated (because, under
// normal circumstances, we have no idea how many vertices a polygon
// will have), so this function ought to be called once all
// vertices have been added.
//
// The function actually computes the plane of the polygon; the normal
// to the polygon will then be the same as the normal to the plane
// of the polygon, so we'll be using that normal whenever necessary.

void
POLYGON::compute_normal() {
	POINT V, W, N, collapse;
	FLOAT fA, fB, fC, fD;

	// compute the normal : (cross product)

	V = *(ppPointArray[1]) - *(ppPointArray[0]);
	W = *(ppPointArray[dNumVertices - 1]) - *(ppPointArray[0]);
	N = V % W;

	// Calculate a, b, c, d 

	fA = N.fX;
	fB = N.fY;
	fC = N.fZ;
	fD = (-1) * (fA * ppPointArray[0]->fX +
		fB * ppPointArray[0]->fY +
		fC * ppPointArray[0]->fZ);

	// Create the associated plane

	pPlane = new PLANE(fA, fB, fC, fD, NULL, NULL);

	// Now compute the collapse information : project the polygon onto an
	// ortographic plane : (the plane is determined by the highest
	// absolute value among fA, fB, fC).

	FLOAT ffA = fabs(fA);
	FLOAT ffB = fabs(fB);
	FLOAT ffC = fabs(fC);

	if (ffA > ffB && ffA > ffC) dCollapse = dX;
	else if (ffB > ffA && ffB > ffC) dCollapse = dY;
	else dCollapse = dZ;

	pCopyArray = new POINT2D[dNumVertices];

	// create polygon image onto that particular plane : 

	for (int copy = 0; copy < dNumVertices; copy++) {

		switch (dCollapse) {
		case dX: pCopyArray[copy].fX = ppPointArray[copy]->fY;
			pCopyArray[copy].fY = ppPointArray[copy]->fZ;
			break;

		case dY: pCopyArray[copy].fX = ppPointArray[copy]->fX;
			pCopyArray[copy].fY = ppPointArray[copy]->fZ;
			break;

		case dZ: pCopyArray[copy].fX = ppPointArray[copy]->fX;
			pCopyArray[copy].fY = ppPointArray[copy]->fY;
			break;

		default: assert(false);
		}
	}
}

// =============================================================== 
// This function will intersect a given ray with a polygon edge
// and return 0 or 1 based on whether the ray does intersect or not.
// The ray starts at pSemi1 and extends infinitely, through pSemi2

int
POLYGON::intersect_edge(POINT2D * pSemi1, POINT2D * pSemi2,
	POINT2D * pSeg1, POINT2D * pSeg2) {
	FLOAT x0 = pSemi1->fX, y0 = pSemi1->fY;
	FLOAT x1 = pSemi2->fX, y1 = pSemi2->fY;

	FLOAT a0 = pSeg1->fX, b0 = pSeg1->fY;
	FLOAT a1 = pSeg2->fX, b1 = pSeg2->fY;

	FLOAT dx = x1 - x0, dy = y1 - y0;
	FLOAT da = a1 - a0, db = b1 - b0;

	FLOAT t0, t1;

	// make sure the segment and the line are not degenerate :

	if ((fabs(da) < EPSILON && fabs(db) < EPSILON) ||
		(fabs(dx) < EPSILON && fabs(dy) < EPSILON))
		return 0;

	// check if the lines are parallel : 

	if (fabs(db * dx - da * dy) < EPSILON) return 0;

	t0 = (da * (y0 - b0) - db * (x0 - a0)) / (db * dx - da * dy);

	if (t0 < 0.0) return 0;

	// check to see that the line does cut the segment : 

	if (fabs(da) < EPSILON) t1 = (y0 - b0 + t0 * dy) / db;
	else t1 = (x0 - a0 + t0 * dx) / da;

	if (t1 < 0.0 || t1 > 1.0) return 0;

	return 1;
}

// =============================================================== 

bool
POLYGON::intersect(RAY * pRay, POINT * pIntersect, POINT * pNormal) {
	FLOAT   xi, yi, zi;		/* Intersection point */
	POINT2D semiStart, semiEnd;
	int     sum;

	// does the ray intersect the plane ? 
	if (!pPlane->intersect(pRay, pIntersect, pNormal))
		return false;

	// if outside the bounding circle, return
	if (pIntersect->distance(pCenter) > fRadius)
		return false;

	// decrement the intersect count because we don't yet
	// know if the line intersects the polygon as well
	pWorld->dIntersect--;

	// Get intersection point 

	xi = pIntersect->fX;
	yi = pIntersect->fY;
	zi = pIntersect->fZ;

	// Now figure out if the point is inside the polygon : 

	// Create semi line starting at intersection point 

	switch (dCollapse) {
	case dX: semiStart.fX = yi; semiStart.fY = zi; break;
	case dY: semiStart.fX = xi; semiStart.fY = zi; break;
	case dZ: semiStart.fX = xi; semiStart.fY = yi; break;
	default: assert(false);
	}

	// Line goes thru middle of the first edge : 

	semiEnd.fX = (pCopyArray[0].fX + pCopyArray[1].fX) * 0.5;
	semiEnd.fY = (pCopyArray[0].fY + pCopyArray[1].fY) * 0.5;

	sum = 1;

	// Intersect it with all the edges and compute parity of the sum.
	// Skip the first edge since we know for sure that'll be hit 

	for (int count = 1; count < dNumVertices - 1; count++)
		sum += intersect_edge(&semiStart, &semiEnd,
			&pCopyArray[count], &pCopyArray[count + 1]);

	sum += intersect_edge(&semiStart, &semiEnd,
		&pCopyArray[dNumVertices - 1], &pCopyArray[0]);

	// if it cuts an odd number of times, the point is outside : return

	if (sum % 2 == 0) return false;

	// We're inside : return 

	pWorld->dIntersect++;
	return true;
}

// =============================================================== 
// ***************************************************************
// **************** WORLD SECTION ********************************
// ***************************************************************
// =============================================================== 

WORLD::WORLD() {
	dHeight = 100;
	dWidth = 100;
	dSuperSample = 0;
	dDepth = 2;

	pEye = NULL;
	pCoi = NULL;
	pViewTransform = NULL;

	pBackground = NULL;
	fAmbient = 0.5;

	pOutputFile = NULL;

	dIntersect = 0;
}

// =============================================================== 

void
WORLD::post_process() {
	if (pEye == NULL) pEye = new POINT(1000.0, 1000.0, 1000.0);
	if (pCoi == NULL) pCoi = new POINT(0.0, 0.0, 0.0);
	if (pViewTransform == NULL)
		pViewTransform = view_transform(pEye, pCoi);

	if (pOutputFile == NULL) pOutputFile = new FILEOUT("output.tga");

	timeStart = time(NULL);

	(*pEye) *= (*pViewTransform);
	(*pCoi) *= (*pViewTransform);
}

// =============================================================== 

WORLD::~WORLD() {
	delete pEye;
	delete pCoi;
	delete pOutputFile;
	delete pViewTransform;

	delete OBJECT::pList;
	delete LIGHT::pList;
	delete PIGMENT::pList;
}

// =============================================================== 

void
WORLD::print_status() {
	cerr << endl << "Image data : " << endl;
	cerr << "\tName : \"" << pWorld->pOutputFile->pName << "\"" << endl;
	cerr << "\tSize : <" << pWorld->dWidth << ", " << pWorld->dHeight << ">" << endl;
	cerr << "\tSupersample : <" << pWorld->dSuperSample << ">" << endl;
	cerr << "\tRay-trace depth : <" << pWorld->dDepth << ">" << endl;
	cerr << endl;
}

// =============================================================== 

void
WORLD::print_statistics() {
	timeEnd = time(NULL);

	int tdiff = (int)timeEnd - (int)timeStart;

	cerr << endl << "Statistics : " << endl;
	cerr << "\tIntersections : " << dIntersect << endl;
	cerr << "\tRendering time : "
		<< tdiff / 3600 << "h:"
		<< (tdiff % 3600) / 60 << "m:"
		<< ((tdiff % 3600) % 60) << "s" << endl;

	cerr << endl;
}

// =============================================================== 
// All these are used when freeing the world objects. We need
// them because we can't take pointers to methods.

void
freeLight(void * item) { delete ((LIGHT*)item); }

// =============================================================== 

void
freePigment(void * item) { delete ((PIGMENT*)item); }

// =============================================================== 

void
freeObject(void * item) { delete ((OBJECT*)item); }

// =============================================================== 

void
object_post_process(void * item) {
	OBJECT * pObj = (OBJECT*)item;

	pObj->post_process();

	// Compute transparency information 
	pObj->pPigment->pSpecs->bTransparent =
		(pObj->pPigment->pSpecs->fOpacity < 1.0 &&
			pObj->pPigment->pSpecs->fEta > 1.0 - EPSILON);
}
