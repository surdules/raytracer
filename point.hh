// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   point.hh :

*/
// ===============================================================

#ifndef __POINT_HH
#define __POINT_HH

#include "transform.hh"

// =============================================================== 
// 2 dimensional point. Used in polygon intersection routines.
// Very rudimentary class

class POINT2D {
public:
	FLOAT fX, fY;

	POINT2D() { fX = 0.0; fY = 0.0; }
	POINT2D(FLOAT x, FLOAT y) { fX = x; fY = y; }
	POINT2D(POINT2D * pPoint) { fX = pPoint->fX; fY = pPoint->fY; }
	~POINT2D() {}
};

// ---------------------------------------------------------------
// 3D point
// The point has 4 coordinates; this is archaic from projective
// rendering, it is only used in order to allow affine transformations
// to be performed on the point (like translations etc.) Normally,
// the 4th coordinate should always be initialized to 1.0

class POINT {
public:
	FLOAT  fX, fY, fZ, fW;                      // point coordinates

	POINT();
	POINT(POINT* pPoint);                       // copy constructor
	POINT(FLOAT x, FLOAT y, FLOAT z);

	~POINT();

	void   normalize();                         // normalize a point (vector)
	FLOAT  distance(POINT* pTo);                // distance between 2 points

	POINT  operator * (TRANSFORM argTrans);     // multiply by a matrix
	POINT  operator *= (TRANSFORM argTrans);    // multiply by a matrix
	FLOAT  operator * (POINT argPoint);         // dot product
	POINT  operator * (FLOAT argFloat);         // scale by a float value

	POINT  operator % (POINT argPoint);         // cross product

	POINT  operator + (POINT argPoint);         // add 2 vectors
	POINT  operator - (POINT argPoint);         // substract 2 vectors

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

#endif // __POINT_HH
