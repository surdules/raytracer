// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   color.hh : Color functions

*/
// ===============================================================

#ifndef __COLOR_HH
#define __COLOR_HH

#include "classes.hh"

// Fill an array of RGB entries with the color of the background
void fill_background(RGBTABLE * pScanLine, int dWidth);

// Color a given pixel given the object, the intersection point 
// and the normal at the intersection point.
void color_pixel(OBJECT * pObject, 
		 POINT * pNormal, POINT * pIntersect,
		 RGBTABLE * pRgbResult);

// Combine the colors computed from local, reflected and
// refracted directions in one color		 
void combine_colors(RGBTABLE * pRgbResult, 
		    RGBTABLE * pLocalColor,
		    RGBTABLE * pReflectedColor,
		    RGBTABLE * pTransmittedColor,
		    OBJECT * pObject);

#endif // __COLOR_HH
