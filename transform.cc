// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   transform.cc : transform functions

*/
// =============================================================== 

#include "classes.hh"
#include "transform.hh"

// =============================================================== 

TRANSFORM::TRANSFORM() {
	init();
}

// =============================================================== 

TRANSFORM::TRANSFORM(TRANSFORM* transform) {
	for (int i = 0; i < T_DIM; i++)
		for (int j = 0; j < T_DIM; j++)
			faValues[i][j] = transform->faValues[i][j];
}

// =============================================================== 

TRANSFORM::TRANSFORM(FLOAT transX, FLOAT transY, FLOAT transZ,
	FLOAT rotX, FLOAT rotY, FLOAT rotZ,
	FLOAT scaleX, FLOAT scaleY, FLOAT scaleZ) {
	init();

	TRANSFORM trans, rot, rotAxis, scale;

	trans.translate(transX, transY, transZ);

	rotAxis.rotate_x(rotX);
	rot = rot * rotAxis;

	rotAxis.rotate_y(rotY);
	rot = rot * rotAxis;

	rotAxis.rotate_z(rotZ);
	rot = rot * rotAxis;

	scale.scale(scaleX, scaleY, scaleZ);

	(*this) = trans * rot * scale;
}

// =============================================================== 
// Rotate about the X axis 

void
TRANSFORM::rotate_x(FLOAT fDegrees) {
	FLOAT radians = fDegrees * DEGREES_TO_RADIANS;

	init();
	faValues[1][1] = cos(radians);
	faValues[1][2] = sin(radians);
	faValues[2][1] = -faValues[1][2];
	faValues[2][2] = faValues[1][1];
}

// =============================================================== 
// Rotate about the Y axis 

void
TRANSFORM::rotate_y(FLOAT fDegrees) {
	FLOAT radians = fDegrees * DEGREES_TO_RADIANS;

	init();
	faValues[0][0] = cos(radians);
	faValues[0][2] = -sin(radians);
	faValues[2][0] = -faValues[0][2];
	faValues[2][2] = faValues[0][0];
}

// =============================================================== 
// Rotation about the Z axis 

void
TRANSFORM::rotate_z(FLOAT fDegrees) {
	FLOAT radians = fDegrees * DEGREES_TO_RADIANS;

	init();
	faValues[0][0] = cos(radians);
	faValues[0][1] = sin(radians);
	faValues[1][0] = -faValues[0][1];
	faValues[1][1] = faValues[0][0];
}


// =============================================================== 
// Initialize a given matrix (to be the identity matrix) 

void
TRANSFORM::init() {
	for (int i = 0; i < T_DIM; i++)
		for (int j = 0; j < T_DIM; j++)
			faValues[i][j] = 0.0;

	for (int i = 0; i < T_DIM; i++)
		faValues[i][i] = 1.0;
}

// =============================================================== 
// Translation matrix 

void
TRANSFORM::translate(FLOAT fXtrans, FLOAT fYtrans, FLOAT fZtrans) {
	init();
	faValues[3][0] = fXtrans;
	faValues[3][1] = fYtrans;
	faValues[3][2] = fZtrans;
}

// =============================================================== 
// Scale matrix : 

void
TRANSFORM::scale(FLOAT fXscale, FLOAT fYscale, FLOAT fZscale) {
	init();

	faValues[0][0] = fXscale;
	faValues[1][1] = fYscale;
	faValues[2][2] = fZscale;
}

// =============================================================== 

TRANSFORM
TRANSFORM::operator * (TRANSFORM argTrans) {
	int i, j, k;
	TRANSFORM product;

	for (i = 0; i < T_DIM; i++)
		for (j = 0; j < T_DIM; j++) {
			product.faValues[i][j] = 0.0;

			for (k = 0; k < T_DIM; k++)
				product.faValues[i][j] += faValues[i][k] *
				argTrans.faValues[k][j];

			if (fabs(product.faValues[i][j]) <= EPSILON)
				product.faValues[i][j] = 0.0;
		}

	return product;
}

// =============================================================== 

TRANSFORM
TRANSFORM::operator * (FLOAT fScale) {
	int i, j;
	TRANSFORM product;

	for (i = 0; i < T_DIM; i++)
		for (j = 0; j < T_DIM; j++) {
			product.faValues[i][j] = fScale * faValues[i][j];

			if (fabs(product.faValues[i][j]) <= EPSILON)
				product.faValues[i][j] = 0.0;
		}

	return product;
}

// =============================================================== 
// compute the determinant of a 3x3 

FLOAT
determinant_3x3(FLOAT a11, FLOAT a12, FLOAT a13,
	FLOAT a21, FLOAT a22, FLOAT a23,
	FLOAT a31, FLOAT a32, FLOAT a33) {
	FLOAT temp;

	temp = a11*a22*a33 +
		a12*a23*a31 +
		a13*a21*a32 -
		a13*a22*a31 -
		a12*a21*a33 -
		a11*a23*a32;

	return (temp);
}

// =============================================================== 
// find the minor of a 3x3 

FLOAT
minor_3x3(TRANSFORM* matrix, int RowCol[6]) {
	return (determinant_3x3(matrix->faValues[RowCol[0]][RowCol[3]],
		matrix->faValues[RowCol[0]][RowCol[4]],
		matrix->faValues[RowCol[0]][RowCol[5]],

		matrix->faValues[RowCol[1]][RowCol[3]],
		matrix->faValues[RowCol[1]][RowCol[4]],
		matrix->faValues[RowCol[1]][RowCol[5]],

		matrix->faValues[RowCol[2]][RowCol[3]],
		matrix->faValues[RowCol[2]][RowCol[4]],
		matrix->faValues[RowCol[2]][RowCol[5]]));
}

// =============================================================== 

void
find_row_col_index(int RowCol[6], int i, int j) {
	int index = 0;

	for (int row = 0; row < 4; row++)
		if (row != i) RowCol[index++] = row;

	for (int row = 0; row < 4; row++)
		if (row != j) RowCol[index++] = row;
}


// =============================================================== 
// invert a 4x4 

TRANSFORM
TRANSFORM::invert() {
	/* | a0 a1 a2 a3 |
   | b0 b1 b2 b3 |
   | c0 c1 c2 c3 |
   | d0 d1 d2 d3 | */

	TRANSFORM Aij;
	TRANSFORM pInverse;
	int RowCol[6], i, j;
	FLOAT fDet;

	/*     | b1 b2 b3
	   det | c1 c2 c3
	   | d1 d2 d3 */

	for (i = 0; i < 4; i++)
		for (j = 0; j < 4; j++) {
			find_row_col_index(RowCol, i, j);
			Aij.faValues[i][j] = minor_3x3(this, RowCol);
		}

	/* find the fDet of matrix by expanding along
   the first column. */

	fDet = 0;
	for (i = 0; i < 4; i++)
		fDet += faValues[i][0] * Aij.faValues[i][0];

	/* Multiply Aij by the appropriate 1,-1 */
	for (i = 0; i < 4; i++)
		for (j = 0; j < 4; j++) {
			if ((i + j) % 2) /*i+j odd */
				Aij.faValues[i][j] *= -1.0;
		}

	/* A^{-1} = transpose(A') / det(A) */

	for (i = 0; i < 4; i++)
		for (j = 0; j < 4; j++) {
			pInverse.faValues[i][j] = Aij.faValues[j][i] / fDet;

			if (fabs(pInverse.faValues[i][j]) < EPSILON)
				pInverse.faValues[i][j] = 0.0;
		}

	return pInverse;
}
