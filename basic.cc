// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   basic.cc : Basic utilities : display error messages etc.

*/
// =============================================================== 

#include "basic.hh"

// =============================================================== 
// display error message and quit

void error(char *mesg1, char *mesg2) {
	cerr << endl << "ERROR: " << mesg1 << " " << mesg2 << endl;
	exit(1);
}

// =============================================================== 
// display error message and quit

void error(char *mesg) {
	cerr << endl << "ERROR: " << mesg << endl;
	exit(1);
}


