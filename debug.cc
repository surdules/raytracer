// =============================================================== 
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   debug.cc : Various functions for debugging/displaying

*/
// =============================================================== 

#ifdef __DEBUG

#include "classes.hh"
#include "debug.hh"
#include "fileio.hh"

// =============================================================== 
// All these functions really shouldn't need any commenting ... :)

// =============================================================== 

void
FILEOUT::print() {
	fprintf(stderr, "OUTPUT FILE : %s\n", pName);
}

// =============================================================== 

void
TRANSFORM::print() {
	int i, j;

	fprintf(stderr, "{");

	for (i = 0; i < T_DIM; i++) {
		for (j = 0; j < T_DIM; j++)
			fprintf(stderr, "%10g\t", faValues[i][j]);
		fprintf(stderr, "\n");
	}

	fprintf(stderr, "}");
}

// =============================================================== 

void
POINT::print() {
	fprintf(stderr, "\tPOINT { %g, %g, %g (%g)}\n",
		fX, fY, fZ, fW);
}

// =============================================================== 

void
RAY::print() {
	fprintf(stderr, "\tRAY {");
	if (pStart != NULL) pStart->print();
	if (pEnd != NULL) pEnd->print();
	fprintf(stderr, "}\n");
}

// =============================================================== 

void
RGB::print() {
	fprintf(stderr, "\tRGB { %g, %g, %g }\n",
		fRed, fGreen, fBlue);
}

// =============================================================== 

void
RGBTABLE::print() {
	fprintf(stderr, "\tRGBTABLE { %d, %d, %d }\n",
		ucRed, ucGreen, ucBlue);
}

// =============================================================== 

void
BUFFER::print() {
	fprintf(stderr, "\tBUFFER { %d, %d }\n",
		dSizeX, dSizeY);
}

// =============================================================== 

void
TEXTURE::print() {
	fprintf(stderr, "\tTEXTURE {");
	switch (cType) {
	case cBUFFER:
		if (uContent.pBuffer != NULL)
			uContent.pBuffer->print();
		break;

	case cFUNCTION:
		fprintf(stderr, "\tFUNCTION-TYPE");
		break;

	default:
		error("Unrecognized texture type\n");
	}

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
COLOR::print() {
	fprintf(stderr, "\tCOLOR {");
	switch (cType) {
	case cRGB:
		if (uContent.pRgb != NULL)
			uContent.pRgb->print();
		break;

	case cTEXTURE:
		if (uContent.pTexture != NULL)
			uContent.pTexture->print();
		break;

	default:
		error("Unrecognized color type\n");
	}
	fprintf(stderr, "}\n");
}

// =============================================================== 

void
SPECS::print() {
	fprintf(stderr, "\tSPECS {\n");

	fprintf(stderr, "\t\tOPACITY : <%g>\n", fOpacity);
	fprintf(stderr, "\t\tREFLECT : <%g>\n", fReflectivity);
	fprintf(stderr, "\t\tETA     : <%g>\n", fEta);
	fprintf(stderr, "\t\tDISTRIB : <%g>\n", fDistribution);
	fprintf(stderr, "\t\tINTENS  : <%g>\n", fIntensity);
	fprintf(stderr, "\t\tSPECUL  : <%g>\n", fSpecularity);

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
PIGMENT::print() {
	fprintf(stderr, "\tPIGMENT {");

	if (pColor != NULL) pColor->print();
	if (pSpecs != NULL) pSpecs->print();

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
BOX::print() {
	fprintf(stderr, "BOX Object {\n");

	assert(p1 != NULL);
	assert(p2 != NULL);
	assert(p3 != NULL);
	assert(ptOrientation != NULL);
	assert(pPigment != NULL);

	p1->print();
	p2->print();
	p3->print();

	ptOrientation->print();
	pPigment->print();

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
ELLIPSOID::print() {
	fprintf(stderr, "ELLIPSOID Object {\n");

	assert(pCenter != NULL);
	assert(ptOrientation != NULL);
	assert(pPigment != NULL);

	pCenter->print();
	fprintf(stderr, "\tRadii : <%g, %g, %g>\n",
		fR1, fR2, fR3);

	ptOrientation->print();
	pPigment->print();

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
SPHERE::print() {
	fprintf(stderr, "SPHERE Object {\n");

	assert(pCenter != NULL);
	assert(ptOrientation != NULL);
	assert(pPigment != NULL);

	pCenter->print();
	fprintf(stderr, "\tRadius : <%g>\n", fRadius);

	ptOrientation->print();
	pPigment->print();

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
CYLINDER::print() {
	fprintf(stderr, "CYLINDER Object {\n");

	assert(pCenter != NULL);
	assert(ptOrientation != NULL);
	assert(pPigment != NULL);

	pCenter->print();
	fprintf(stderr, "\tRadius/Height : <%g, %g>\n", fRadius, fHeight);

	ptOrientation->print();
	pPigment->print();

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
TORUS::print() {
	fprintf(stderr, "TORUS Object {\n");

	assert(pCenter != NULL);
	assert(ptOrientation != NULL);
	assert(pPigment != NULL);

	pCenter->print();
	fprintf(stderr, "\tRadius1/Radius2 : <%g, %g>\n",
		fBigRadius, fSmallRadius);

	ptOrientation->print();
	pPigment->print();

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
CONE::print() {
	fprintf(stderr, "CONE Object {\n");

	assert(pCenter != NULL);
	assert(ptOrientation != NULL);
	assert(pPigment != NULL);

	pCenter->print();
	fprintf(stderr, "\tRadius/Height : <%g, %g>\n", fRadius, fHeight);

	ptOrientation->print();
	pPigment->print();

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
PLANE::print() {
	fprintf(stderr, "PLANE Object {\n");

	fprintf(stderr, "\t<a,b,c,d> : <%g, %g, %g, %g>\n",
		fA, fB, fC, fD);

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
POLYGON::print() {
	fprintf(stderr, "POLYGON Object {\n");

	fprintf(stderr, "\tVertices : <%d>\n", dNumVertices);

	if (dNumVertices != 0) {
		assert(ppPointArray != NULL);

		for (int count = 0; count < dNumVertices; count++) {
			assert(ppPointArray[count] != NULL);
			ppPointArray[count]->print();
		}
	}

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
LIGHT::print() {
	fprintf(stderr, "\tLIGHT {");

	fprintf(stderr, "\tIntensity : <%g>\n", fIntensity);
	if (pSource != NULL) pSource->print();
	if (pRgb != NULL) pRgb->print();

	fprintf(stderr, "}\n");
}

// =============================================================== 

void
WORLD::print() {
	fprintf(stderr, "WORLD {\n");

	if (pEye != NULL) pEye->print();
	if (pCoi != NULL) pCoi->print();
	if (pViewTransform != NULL) pViewTransform->print();

	fprintf(stderr, "\tSize X/Y : <%d, %d>\n",
		dHeight, dWidth);

	fprintf(stderr, "\tSuper : <%d>\n",
		dSuperSample);

	if (pBackground != NULL) pBackground->print();
	fprintf(stderr, "\tAmbient : < %g >\n", fAmbient);

	if (pOutputFile != NULL) pOutputFile->print();

	fprintf(stderr, "\n*LIGHT SECTION* : \n");
	LIGHT::pList->mapcar(printLight);

	fprintf(stderr, "\n*PIGMENT SECTION* : \n");
	PIGMENT::pList->mapcar(printPigment);

	fprintf(stderr, "\n*OBJECT SECTION* : \n");
	OBJECT::pList->mapcar(printObject);

	fprintf(stderr, "}\n");
}

// =============================================================== 
// Used in 'mapcar'ing over the linked lists of objects, lights
// and pigments : each one of these functions simply prints
// the associated object (we have to create these sepparate
// functions because it's impossible to take the address
// of a method)

void
printLight(void * item) { ((LIGHT*)item)->print(); }

// =============================================================== 

void
printPigment(void * item) { ((PIGMENT*)item)->print(); }

// =============================================================== 

void
printObject(void * item) { ((OBJECT*)item)->print(); }


#endif
