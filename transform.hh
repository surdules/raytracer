// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   transform.hh : math includes

*/
// =============================================================== 

#ifndef __MATHLIB_HH
#define __MATHLIB_HH

#include "basic.hh"

class TRANSFORM {
public:
    FLOAT faValues[T_DIM][T_DIM];

    TRANSFORM  ();
    TRANSFORM  (TRANSFORM* transform);
    TRANSFORM  (FLOAT transX, FLOAT transY, FLOAT transZ,
		FLOAT rotX, FLOAT rotY, FLOAT rotZ,
		FLOAT scaleX, FLOAT scaleY, FLOAT scaleZ);
   ~TRANSFORM  () {}

    void       init();
    void       rotate_x(FLOAT fDegrees);
    void       rotate_y(FLOAT fDegrees);
    void       rotate_z(FLOAT fDegrees);
    void       scale(FLOAT fXscale, FLOAT fYscale, FLOAT fZscale);
    void       translate(FLOAT fXtrans, FLOAT fYtrans, FLOAT fZtrans);

    TRANSFORM invert();

    TRANSFORM operator * (FLOAT fScalar);
    TRANSFORM operator * (TRANSFORM argTrans);
    TRANSFORM operator - (TRANSFORM argTrans);
    TRANSFORM operator + (TRANSFORM argTrans);

#ifdef __DEBUG
    void print();
#endif

};


FLOAT determinant_3x3(FLOAT a11,FLOAT a12,FLOAT a13,
		      FLOAT a21,FLOAT a22,FLOAT a23,
		      FLOAT a31,FLOAT a32,FLOAT a33);
FLOAT minor_3x3(TRANSFORM* matrix, int RowCol[6]);
void  find_row_col_index(int RowCol[6], int i, int j);

#endif

