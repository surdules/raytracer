// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   viewmath.cc :

*/
// =============================================================== 

#include "classes.hh"
#include "transform.hh"
#include "viewmath.hh"

// =============================================================== 
// Compute the viewing transformation 

TRANSFORM*
view_transform(POINT* eye, POINT* coi) {
	TRANSFORM  *pTrans = new TRANSFORM();
	TRANSFORM  trans1, trans2;
	POINT      newCoi, oldCoi;
	FLOAT      fCos, fSin, fZero;
	FLOAT      fScrDist;

	fScrDist = eye->distance(coi);

	trans1.translate(-eye->fX, -eye->fY, -eye->fZ);
	newCoi = (*coi) * trans1;

	/* Project the newCoi on the xz plane */
	newCoi.fY = 0;
	newCoi.normalize();

	fZero = newCoi.fX * newCoi.fX +
		newCoi.fY * newCoi.fY +
		newCoi.fZ * newCoi.fZ;

	/* Check if already on the y-z plane. If so, skip this step */
	if (fZero != 0) {
		/* newCoi (DOT) z-axis = cos(theta)  -- This is just the z-coord */
		fCos = newCoi.fZ;
		fSin = sqrt(1 - (fCos * fCos));

		/* To check the sign of fSin, we notice that if
		   the x coord of the newCoi vector is negative, then
		   the angle b/w newCoi and z-axis is > 180 degrees -> negative */

		if (newCoi.fX < 0) fSin = -fSin;

		move_to_yz(&trans2, fCos, -fSin);
		*pTrans = trans1 * trans2;
	} else (*pTrans) = trans1;

	oldCoi = newCoi;
	newCoi = (*coi) * (*pTrans);

	newCoi.normalize();

	fCos = newCoi.fZ;
	fSin = sqrt(1 - (fCos * fCos));

	/* To check the sign of fSin, we notice that if
	   the x coord of the newCoi vector is negative, then
	   the angle b/w newCoi and z-axis is > 180 degrees -> negative */

	if (newCoi.fY > 0) fSin = -fSin;

	move_to_z(&trans1, fCos, -fSin);

	trans2 = (*pTrans) * trans1;
	trans1.translate(0, 0, -fScrDist);
	(*pTrans) = trans2 * trans1;

	return pTrans;
}

// =============================================================== 
/* Produce a matrix that is effectively a transposition
   to the YZ plane */

void
move_to_yz(TRANSFORM *pTrans, FLOAT fCos, FLOAT fSin) {
	pTrans->init();
	pTrans->faValues[0][0] = fCos;
	pTrans->faValues[0][2] = -fSin;
	pTrans->faValues[2][0] = fSin;
	pTrans->faValues[2][2] = fCos;
}

// =============================================================== 
/* Produce a matrix that is effectively a transposition
   to the Z plane */

void
move_to_z(TRANSFORM *pTrans, FLOAT fCos, FLOAT fSin) {
	pTrans->init();
	pTrans->faValues[1][1] = fCos;
	pTrans->faValues[1][2] = fSin;
	pTrans->faValues[2][1] = -fSin;
	pTrans->faValues[2][2] = fCos;
}

