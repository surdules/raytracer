// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   render.hh : main rendering functions and externs

*/
// =============================================================== 

#ifndef __RENDER_HH
#define __RENDER_HH

extern WORLD * pWorld;

extern FILE* yyin;
extern int   yylineno;

void yyerror(char *s);
int  yylex();
int  yyparse(void);

void initialize();
void main_routine();
void cleanup();

int intersect_all(RAY * pRay,
	OBJECT ** ppObject,
	POINT * pIntersect, POINT * pNormal);

void trace_ray(RAY * pRay, int dDepth, RGBTABLE * pRgbResult);

#endif       /* __RENDER_HH */
