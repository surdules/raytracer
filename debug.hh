// =============================================================== 
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   debug.hh : Functions for debugging

*/
// =============================================================== 

#ifdef __DEBUG

#ifndef __DEBUG_HH
#define __DEBUG_HH

#include "classes.hh"

void printLight(void * item);
void printPigment(void * item);
void printObject(void * item);

#endif // __DEBUG_HH

#endif // __DEBUG 
