#// ===============================================================
#/*
#   Ray tracer project
#
#   Razvan Surdulescu
#   Summer 1996
#
#   Makefile
#
#   Other flags may be added as follows to CPPFLAGS :
#      -D__DEBUG   : enable debugging code : print all objects parsed in
#                    and the status of the environment
#      -D__ASSERT :  enable asserts in the code 
#
#   The purified target can only be built on the Sun workstations at
#   present. 
#
#   Code dependencies don't work properly for 'parser.y' and 'lexer.l'
#   You need to rebuild them as necessary by hand whenever modifying
#   *any* of the include files (especially classes.hh)
#*/
#// =============================================================== 

CC        = g++

CPPFLAGS  = -g -pg -Wall -D__ASSERT
#CPPFLAGS  = -O6 -m486 -Wall -pipe -finline-functions -fstrength-reduce \
                -malign-loops=2 -malign-jumps=2 -malign-functions=2

OFLAGS    = -pg
LIB       = -lm -lfl

INCLUDES = basic.hh\
	light.hh\
	debug.hh\
	llist.hh\
	point.hh\
	transform.hh\
	fileio.hh\
	objects.hh\
	viewmath.hh\
	classes.hh\
	defs.hh\
	color.hh\
	render.hh

SOURCES  = basic.cc\
	light.cc\
	texture.cc\
	debug.cc\
	llist.cc\
	point.cc\
	transform.cc\
	fileio.cc\
	objects.cc\
	viewmath.cc\
	color.cc\
	ray.cc\
	render.cc

TSOURCES = parser.c parser.h lexer.c

SUPPORT  = parser.y lexer.l

OBJECTS  = parser.o\
	lexer.o\
	basic.o\
	light.o\
	texture.o\
	debug.o\
	llist.o\
	point.o\
	transform.o\
	fileio.o\
	objects.o\
	viewmath.o\
	color.o\
	ray.o\
	render.o

TARGET   = render

# Target --------------------------

$(TARGET):	$(OBJECTS)
	$(CC) $(OFLAGS) -o $(TARGET) $(OBJECTS) $(LIB)

parser.c:	parser.y
	bison -o parser.c -d --debug parser.y

lexer.o:	lexer.c
	$(CC) -c lexer.c

lexer.c:	lexer.l
	flex -olexer.c lexer.l

# Utilities ------------------------

# Print up the entire source tree
#
print:
	enscript -2rG -pprint.ps $(SOURCES) $(INCLUDES) $(SUPPORT) Makefile

# Create code dependencies 
#
depend: $(SOURCES) $(INCLUDES)
	$(CC) $(CPPFLAGS) -M $(SOURCES) > makedep
	echo '/^# DO NOT DELETE THIS LINE/+2,$$d' >eddep
	echo '$$r makedep' >>eddep
	echo 'w' >>eddep
	ed - Makefile < eddep
	rm eddep makedep
	echo '# DEPENDENCIES MUST END AT END OF FILE' >> Makefile
	echo '# IF YOU PUT STUFF HERE IT WILL GO AWAY' >> Makefile
	echo '# see make depend above' >> Makefile

# Cleanup
#
clean:
	rm -rf core *~ *.o $(TSOURCES) $(TARGET)

#-----------------------------------------------------------------
# DO NOT DELETE THIS LINE -- make depend uses it
# DEPENDENCIES MUST END AT END OF FILE
# DEPENDENCIES MUST END AT END OF FILE
# IF YOU PUT STUFF HERE IT WILL GO AWAY
# see make depend above
