// =============================================================== 
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   render.cc : the main modules of the raytracer

*/
// =============================================================== 

#include "classes.hh"
#include "render.hh"
#include "viewmath.hh"
#include "color.hh"
#include "light.hh"
#include "point.hh"

WORLD * pWorld;

LLIST * PIGMENT::pList;
LLIST * OBJECT::pList;
LLIST * LIGHT::pList;

// =============================================================== 

int
main(int argc, char* argv[]) {
	++argv, --argc;  // skip over program name 
	if (argc > 0) {
		if (fopen_s(&yyin, argv[0], "r") != 0) {
			error("Can't open output file", argv[0]);
		}
	} else {
		yyin = stdin;
	}

	initialize();

	cerr << "Parsing ... ";
	yyparse();
	cerr << "Done." << endl;

	main_routine();

	cleanup();

	if (yyin != stdin) fclose(yyin);
}

// =============================================================== 

void
initialize() {
	pWorld = new WORLD();

	PIGMENT::pList = new LLIST(freePigment);
	OBJECT::pList = new LLIST(freeObject);
	LIGHT::pList = new LLIST(freeLight);
}

// =============================================================== 
// Adapted from Alan Watt : 3D Computer Graphics

void
trace_ray(RAY * pRay, int dDepth, RGBTABLE * pRgbResult) {
	RGBTABLE localColor, reflectedColor, transmittedColor;
	RAY      reflectedRay, transmittedRay;
	POINT    intersect, normal;
	OBJECT * pObject;
	SPECS  * pSpecs;

	if (!intersect_all(pRay, &pObject,
		&intersect, &normal)) {
		fill_background(pRgbResult, 1);
	} else {
		POINT I, R, T;
		FLOAT costh;

		color_pixel(pObject, &normal, &intersect, &localColor);

		if (dDepth >= pWorld->dDepth) {
			*pRgbResult = localColor;
		} else {
			normal.normalize();
			I = intersect - *(pWorld->pEye);
			I.normalize();

			// need to multiply by -1 because I and normal have different
			// directions
			costh = (-1.0) * (I * normal);

			// Reflected direction 

			R = I + normal * (2.0 * costh);

			reflectedRay.pStart = new POINT(&intersect);
			reflectedRay.pEnd = new POINT();
			reflectedRay.bInside = pRay->bInside;

			// displace the start point up a little bit so we don't self 
			// intersect

			*(reflectedRay.pStart) = *(reflectedRay.pStart) +
				(R * JITTER);
			*(reflectedRay.pEnd) = *(reflectedRay.pStart) + R;

			trace_ray(&reflectedRay, dDepth + 1, &reflectedColor);

			// Transmitted direction : 

			pSpecs = pObject->pPigment->pSpecs;

			// Need to compute the transmitted direction only if
			// the object is transparent (see object_post_process
			// and the SPECS class

			if (pSpecs->bTransparent) {

				FLOAT eta, costh2;

				// Figure out if the ray is leaving or entering the
				// object (*NOTE* : assume that one medium is ALWAYS
				// air (i.e. eta_air = 1.0)

				if (pRay->bInside) {
					eta = 1.0 / pSpecs->fEta;

					// if the ray is on the inside, then the normal is 
					// inverted as well as the <I, N> angle ?

					costh *= (-1.0);
					normal = normal * (-1.0);
				} else {
					eta = pSpecs->fEta;
				}

				costh2 = eta * sqrt(1 - 1 / (eta * eta) *
					(1 - costh * costh));

				T = (I * (1.0 / eta)) -
					(normal * (costh2 - 1.0 / eta * costh));

				transmittedRay.pStart = new POINT(&intersect);
				transmittedRay.pEnd = new POINT();
				transmittedRay.bInside = !(pRay->bInside);

				// displace the start point a little bit so we don't self 
				// intersect

				*(transmittedRay.pStart) = *(transmittedRay.pStart) +
					(T * JITTER);
				*(transmittedRay.pEnd) = *(transmittedRay.pStart) + T;

				trace_ray(&transmittedRay, dDepth + 1, &transmittedColor);
			}
		}

		combine_colors(pRgbResult,
			&localColor, &reflectedColor, &transmittedColor,
			pObject);
	}
}

// =============================================================== 

int
intersect_all(RAY * pRay,
	OBJECT ** ppObject,
	POINT * pIntersect, POINT * pNormal) {
	POINT    intersect, normal, scrapIntersect, scrapNormal;
	OBJECT * pObject = NULL, *pScrapObject = NULL;
	int      result = 0, first = 1;

	for (ELEMENT *ptrOb = OBJECT::pList->first;
		ptrOb != NULL;
		ptrOb = ptrOb->next) {

		pScrapObject = ((OBJECT*)ptrOb->item);

		if (pScrapObject->intersect(pRay, &scrapIntersect, &scrapNormal)) {

			result = 1;

			if (first ||
				(pWorld->pEye->distance(&scrapIntersect) <
					pWorld->pEye->distance(&intersect))) {

				intersect = scrapIntersect;
				normal = scrapNormal;
				pObject = pScrapObject;

				if (first) first = 0;
			}
		}
	}

	if (pObject) {
		if (ppObject) *ppObject = pObject;
		if (pIntersect) *pIntersect = intersect;
		if (pNormal) *pNormal = normal;
	}

	return result;
}

// =============================================================== 

void
main_routine() {
	pWorld->post_process();
	pWorld->print_status();

#ifdef __DEBUG
	pWorld->print();
#endif

	OBJECT::pList->mapcar(object_post_process);
	LIGHT::pList->mapcar(light_post_process);

	int dWidth = pWorld->dWidth;
	int dHeight = pWorld->dHeight;
	int count, point;

	RGBTABLE * pScanLine = new RGBTABLE[dWidth];

	RGBTABLE rgbResult;
	RAY ray(new POINT(pWorld->pEye), new POINT(), false);

	pWorld->pOutputFile->write_header(dWidth, dHeight);

	cerr << "\rProgress : [0%]" << " Line 0 of " << dHeight;

	for (count = dHeight - 1; count >= 0; count--) {

		if ((dHeight - count) % 10 == 0) {
			cerr << "\rProgress : ["
				<< ((dHeight - count) * 100) / dHeight << "%]"
				<< " Line " << dHeight - count << " of " << dHeight;
		}

		for (point = 0; point < dWidth; point++) {

			ray.pEnd->fX = (point - dWidth / 2);
			ray.pEnd->fY = (count - dHeight / 2);

			trace_ray(&ray, 1, &rgbResult);

			pScanLine[point] = rgbResult;
		}

		pWorld->pOutputFile->write_scanline(pScanLine, dWidth);
	}

	delete[] pScanLine;

	cerr << endl;

	pWorld->print_statistics();
}

// =============================================================== 

void
cleanup() {

	delete pWorld;
}

