
/* Test file for the renderer : */

/*
	General comments :

1 : Orientation :

       ^ Y-axis
       |
       |
       |
       +-------> Z-axis
      /
     /
    / X-axis


2 : Parameters :

Order of color specifications :
 
    Ambient
    Diffuse
         
    Reflect
    Refract
                     
    Eta  
    Opacity
 
    Distribution
    Specularity

*/

/* 

Define testPigment = Pigment { Rgb <1.0, 1.0, 1.0> } 

*/

/* WORLD section : */

World {

	Camera {

		Eye < 1400.0, 300.0, 1200.0 > 
		Coi < 0.0, 0.0, 0.0 >

	}

	Size <500, 500>

	Output " image0.tga "

	Supersample < 0 >

	Depth < 2 >

	Background Pigment { Rgb <0.0, 0.0, 0.0> } 

	Ambient <0.5>
}

/* LIGHTS section : */

Lights { 

	PointLight { <700.0, 0.0, 0.0>
		     Intensity <0.5>
        }

	PointLight { <0.0, 700.0, 0.0>
		     Intensity <1.0>
        }

	PointLight { <0.0, 0.0, 700.0>
		     Intensity <0.5>
        }
} 

/* OBJECTS Section : */

Sphere {

	<0.0, 0.0, 0.0>
	<100.0>

	Pigment { Rgb <0.0, 1.0, 0.0> 
	          Specs {
			Ambient <0.2>
			Diffuse <0.4>

			Reflect <0.6>

			Distribution <100.0>
			Specularity <0.8>
	          }
	}
}

Sphere {

	<100.0, 0.0, 300.0>
	<60.0>

	Pigment { Rgb <1.0, 0.0, 0.0> 
	          Specs {
			Ambient <0.4>
			Diffuse <0.6>

			Reflect <0.8>

			Distribution <200.0>
			Specularity <0.8>
	          }
	}
}

Sphere {

	<250.0, 0.0, 0.0>
	<40.0>

	Pigment { Rgb <1.0, 1.0, 0.0> 
	          Specs {
			Ambient <0.4>
			Diffuse <0.6>

			Reflect <0.3>

			Distribution <100.0>
			Specularity <0.8>
	          }
	}
}

Polygon {

	<-200.0, -200.0, -110.0>
	< 200.0, -200.0, -110.0>
	< 200.0,  200.0, -110.0>
	<-200.0,  200.0, -110.0>

	Pigment { Rgb <1.0, 1.0, 0.0> 
	          Specs {
			Ambient <0.0>
			Diffuse <0.9>

			Reflect <0.3>

			Distribution <400.0>
			Specularity <0.3>
	          }
	}
}

Polygon {

	<-200.0, -110.0,   0.0>
	<-200.0, -110.0, 400.0>
	< 200.0, -110.0, 400.0>
	< 200.0, -110.0,   0.0>

	Pigment { Rgb <0.5, 0.5, 0.5> 
	          Specs {
			Ambient <0.0>
			Diffuse <0.9>

			Reflect <0.3>

			Distribution <400.0>
			Specularity <0.3>
	          }
	}
}

Plane {
	
	<-1.0, 0.0, 0.0, -500.0>

	Pigment { Rgb <1.0, 0.5, 0.0> 
	          Specs {
			Ambient <0.3>
			Diffuse <0.1>

			Reflect <1.0>

			Distribution <100.0>
			Specularity <0.5>
	          }
	}
}

Box {

	<200.0, -150.0, 200.0>
	<300.0, -50.0, 300.0>

	Pigment { Rgb <1.0, 0.5, 0.0> 
	          Specs {
			Ambient <0.4>
			Diffuse <0.4>

			Reflect <0.5>

			Distribution <100.0>
			Specularity <0.5>
	          }
	}
}

