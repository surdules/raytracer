/* =============================================================== */
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   lexer.l : The lexer

*/
/* =============================================================== */

%{

#include "classes.hh"
#include "render.hh"
#include "parser.tab.h"

%}

%option yylineno
%option noyywrap

DIGIT		[0-9]
INT		"-"?{DIGIT}+
LFLOAT		"-"?{DIGIT}*"."{DIGIT}*

/* this is really an alphanumeric string that can contain
   the necessary extra characters permitted in file names : '/' and '.' */

PATH		[~\./A-Za-z0-9][~\./A-Za-z0-9]*

%%

{LFLOAT}	{
		  yylval.tFLOAT = atof(yytext);
		  return LFLOAT;
		}

{INT}		{
		  yylval.tINT = atoi(yytext);
		  return INT;
		}

Define 		{ return LDEFINE; }
World 	        { return LWORLD; }
Camera          { return LCAMERA; }
Lights		{ return LLIGHTS; } 
Eye		{ return LEYE; }
Coi		{ return LCOI; }
Size		{ return LSIZE; }
Output		{ return LOUTPUT; }
Supersample	{ return LSUPERSAMPLE; }
Depth    	{ return LDEPTH; }
Background	{ return LBACKGROUND; }

PointLight	{ return LPOINTLIGHT; } 
Intensity	{ return LINTENSITY; } 

Ambient		{ return LAMBIENT; }
Diffuse         { return LDIFFUSE; } 

Reflect         { return LREFLECT; }
Refract         { return LREFRACT; }

Eta		{ return LETA; }
Opacity 	{ return LOPACITY; }

Distribution	{ return LDISTRIBUTION; } 
Specularity	{ return LSPECULARITY; }

Color		{ return LCOLOR; }
Pigment		{ return LPIGMENT; }
Specs		{ return LSPECS; }

Translate	{ return LTRANSLATE; } 
Rotate		{ return LROTATE; }
Scale		{ return LSCALE; }
Transform	{ return LTRANSFORM; }
Rgb		{ return LRGB; }
Texture		{ return LTEXTURE; }

Mudmap          { return LTEXMUD; }

Box		{ return LBOX; }
Ellipsoid	{ return LELLIPSOID; } 
Sphere		{ return LSPHERE; }
Cylinder	{ return LCYLINDER; } 
Torus		{ return LTORUS; }
Plane		{ return LPLANE; }
Polygon		{ return LPOLYGON; } 
Cone		{ return LCONE; }

"{"|"}"     return *yytext;
"<"|">"     return *yytext;
","         return *yytext;
"="         return *yytext;
"\""        return *yytext;

{PATH}      { yylval.ptCHAR = new char[strlen(yytext) + 1];
              strcpy_s(yylval.ptCHAR, strlen(yytext) + 1, yytext);
              return STRING;
            }

"/*"        {
            register int c;

            for ( ; ; )
	    {
	            while ( (c = yyinput()) != '*' &&
			     c != EOF )
					;    /* eat up text of comment */

		    if ( c == '*' )
		    {
		            while ( (c = yyinput()) == '*' ) ;
			    if ( c == '/' )
                                   break;    /* found the end */
                    }

                    if ( c == EOF )
		    {
		           yyerror( "EOF in comment" );
			           break;
		    }
	    }
            }


[ \t\n]+    /* eat up whitespace */

.	    {
                  /* Any other character is an error; the parser will
		     not recognize the ERR token and will produce its own
		     error */
	
        	  fprintf(stderr,  "Unrecognized character: %s\n", yytext );
		  return ERR;
	    }

%%

