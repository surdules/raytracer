// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   classes.hh : All the main classes used in the program

*/
// =============================================================== 

#ifndef __CLASSES_HH
#define __CLASSES_HH

#include "basic.hh"

#include "llist.hh"
#include "transform.hh"
#include "point.hh"
#include "fileio.hh"

void freeLight(void * item);
void freePigment(void * item);
void freeObject(void * item);

void object_post_process(void * item);

// =============================================================== 
// Object types-defines 

// These are numeric identifiers for various features. Some classes
// contain a 'cType' field which has to be one of these values.

enum {
	cRGB = 0,
	cTEXTURE = 1
};

enum {
	cBUFFER = 0,
	cFUNCTION = 1,
};

// These are used internally in polygon intersection routines

enum {
	dX = 0,
	dY = 1,
	dZ = 2
};

// =============================================================== 
// Unless otherwise specified, all classes free their dynamically
// allocated data members in their destructors.

// ---------------------------------------------------------------
// Ray class : used in intersections. The ray starts at pStart
// and extends infinitely past pEnd. The 2 points define the ray
// uniquely

class RAY
{
public:
	POINT*    pStart;                           // start point/end point
	POINT*    pEnd;

	bool      bInside;                          // is the ray inside the object ?
												// (used for transmitted rays)

	RAY();
	RAY(POINT* pS, POINT* pE, bool inside);
	RAY(RAY* pR);                               // copy constructor

	~RAY();

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Rgb class : contains red, green, blue values in between 0.0 and 1.0

class RGB
{
public:
	FLOAT    fRed, fGreen, fBlue;              // r,g,b values

	RGB();
	RGB(FLOAT r, FLOAT g, FLOAT b);
	RGB(RGB * pRgb);                           // copy constructor

	~RGB();

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Generic buffer class : used to hold the contexts of a user
// loaded texture (from TGA, GIF etc.)

class BUFFER
{
public:
	char*    pcBuffer;                        // buffer contents
	int      dSizeX, dSizeY;                  // buffer size

	BUFFER();
	BUFFER(BUFFER * pBuffer);                 // copy constructor
	BUFFER(char * pcName);                    // which file to load from

	~BUFFER();

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Texture class : union of user loaded texture or functional texture
// The 'cType' field identifies which type of texture are we
// dealing with.

class TEXTURE
{
public:
	char  cType;                              // type of texture

	union {
		BUFFER*     pBuffer;                  // texture buffer
		RGB(*pProcedure) (POINT* pP);       // functional texture
	} uContent;

	TEXTURE();
	TEXTURE(char * pcName);                   // texture to load from file
	TEXTURE(BUFFER * pBuffer);                // texture from buffer
	TEXTURE(TEXTURE * pTexture);              // copy constructor

	~TEXTURE();

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// Procedural textures : 

RGB tex_mud(POINT * pPoint);

// ---------------------------------------------------------------
// Generic Color class : can be either an RGB value or a texture
// The 'cType' field identifies the type. 

class COLOR
{
public:
	char     cType;                           // type of color

	union {
		RGB*   pRgb;                      // RGB type
		TEXTURE* pTexture;                // texture type
	} uContent;

	COLOR();
	COLOR(RGB * pRgb);                       // RGB constructor
	COLOR(TEXTURE * pTexture);               // texture constructor
	COLOR(COLOR * pColor);                   // copy constructor

	~COLOR();

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Specs class : contains various specifications for describing
// a color.
//
// fAmbient, fDiffuse : how much ambient/diffuse light the object reflects
// fReflect, fRefract : how much light from interobject reflections/refractions
//                      the object passes on
// fEta         : the diffraction coefficient; determines direction of 
//                of refracted light (that goes thru the object)
// fOpacity     : how much light the object allows to pass through
//                0.0 = totally opaque, 1.0 = totally transparent
// fDistribution: how large the specular spot will be on the object
//                ->0        = completely distributed, large spot
//                ->Infinity = focused spot
// fSpecularity : how much specular light is reflected from the object
//                0.0 = no specular light is reflected,
//                1.0 = all specular light is reflected

class SPECS
{
public:
	FLOAT    fAmbient;
	FLOAT    fDiffuse;

	FLOAT    fReflect;
	FLOAT    fRefract;

	FLOAT    fEta;
	FLOAT    fOpacity;
	bool     bTransparent;

	FLOAT    fDistribution;
	FLOAT    fSpecularity;

	SPECS();
	SPECS(FLOAT amb, FLOAT diff, FLOAT refl, FLOAT refr, FLOAT eta, FLOAT op,
		FLOAT dist, FLOAT spec);
	SPECS(SPECS * pSpecs);

	~SPECS();

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Generic pigment class : this is the most generic way of
// specifying the color of an object. It contains a COLOR and
// SPECS for that particular COLOR. 
//
// Contains a static linked list on which all pigments are placed.
// This is such that pigments can be 'define'd in the parser and
// then reused from the linked list by various objects; the pigments
// such defined are identified via their name.

class PIGMENT
{
public:
	static    LLIST* pList;                          // linked list of pigments
	char    * pName;                                 // name of each pigment

	COLOR    *pColor;                                // 'color' of pigment
	SPECS    *pSpecs;                                // specs for the color

	PIGMENT();
	PIGMENT(COLOR * color, SPECS * specs);
	PIGMENT(FLOAT fRed, FLOAT fGreen, FLOAT fBlue);  // create an RGB pigment
	PIGMENT(PIGMENT * pigment);                      // copy constructor

	~PIGMENT();

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// =============================================================== 
// Generic object class : all objects inherit from this class
// It contains a linked list of objects, the type of each object
// the pigment and a transformation which specifies how the
// object is to be rotated/translated/scaled
//
// Virtual functions :
//
//      intersect    : says if the ray intersects the object or not.
//                     returns the intersection point and the normal
//                     at the intersection point
//                     pIntersect and pNormal must point to allocated
//                     and valid areas of memory in which the resulting
//                     intersection point and normal are copied.
//
//      post_process : this function is to be called only after
//                     the object has been parsed entirely from 
//                     the file; it basically applies the view
//                     transformation to the object and computes
//                     other components of the object as necessary
//                     (such as normals in polygons etc.)
//                     The caller can also specify a custom
//                     transformation if he wishes (by default,
//                     the transformation is set to NULL).
//
// The 'bAdd' parameter in the constructor basically specifies
// if the given object is to be added on the list of objects or not.
// This is done in order to allow 'internal' objects (for instance,
// a POLYGON object contains a PLANE object inside it. However, the
// PLANE object is only to be considered for intersection whenever
// the POLYGON to which it belongs is intersected. We don't want
// the PLANE object to be added on the linked list of objects
// normally considered for intersection). By default, objects
// are not added on the list; inside the parser routine, all calls
// to the object constructors are made with 'bAdd' set to 'true'
// so that objects are added on the list.

class OBJECT
{
public:
	static    LLIST* pList;             // static list of objects

	PIGMENT*   pPigment;                // object pigment
	TRANSFORM* ptOrientation;           // object orientation

	OBJECT(bool bAdd = false);
	virtual ~OBJECT();

	virtual bool intersect(RAY * pRay, POINT * pIntersect,
		POINT * pNormal) = 0;

	virtual void post_process(TRANSFORM * pTrans = NULL) = 0;

#ifdef __DEBUG
	virtual void print() = 0;
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Box class : defines a rectangular box, by specifying two diagonally
// opposed corners. 
// Once the box is constructed, the ppFaces array will be an array
// of polygons, each of which determines a face of the box.

class POLYGON;
class SPHERE;

class BOX : public OBJECT
{
public:
	POINT      *p1, *p2;                              // diagonal points
	POLYGON    *ppFaces[B_FACE];                      // array of faces
	SPHERE     *pbSphere;                             // bounding sphere

	BOX(bool bAdd = false);
	BOX(POINT * po1, POINT * po2,
		PIGMENT * pigment, TRANSFORM * orient,
		bool bAdd = false);
	~BOX();

	bool intersect(RAY * pRay, POINT * pIntersect,
		POINT * pNormal);

	void compute_faces();                             // compute the faces
	void post_process(TRANSFORM * pTrans = NULL);     // post process

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Ellipsoid object : defined by a center and 3 radii.

class ELLIPSOID : public OBJECT
{
public:
	POINT*     pCenter;
	FLOAT      fR1, fR2, fR3;

	ELLIPSOID(bool bAdd = false);
	ELLIPSOID(POINT * center, FLOAT r1, FLOAT r2, FLOAT r3,
		PIGMENT * pigment, TRANSFORM * orient,
		bool bAdd = false);
	~ELLIPSOID();

	bool intersect(RAY * pRay, POINT * pIntersect,
		POINT * pNormal);

	void post_process(TRANSFORM * pTrans = NULL);

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Sphere object : defined by a center and a radius

class SPHERE : public OBJECT
{
public:
	POINT*     pCenter;
	FLOAT      fRadius;

	SPHERE(bool bAdd = false);
	SPHERE(POINT * center, FLOAT radius,
		PIGMENT * pigment, TRANSFORM * orient,
		bool bAdd = false);
	~SPHERE();

	bool intersect(RAY * pRay, POINT * pIntersect,
		POINT * pNormal);

	void post_process(TRANSFORM * pTrans = NULL);

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Cylinder object : determined by a center, radius and height

class CYLINDER : public OBJECT
{
public:
	POINT*     pCenter;
	FLOAT      fRadius, fHeight;

	CYLINDER(bool bAdd = false);
	CYLINDER(POINT * center, FLOAT radius, FLOAT height,
		PIGMENT * pigment, TRANSFORM * orient,
		bool bAdd = false);
	~CYLINDER();

	bool intersect(RAY * pRay, POINT * pIntersect,
		POINT * pNormal);

	void post_process(TRANSFORM * pTrans = NULL);

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Torus class : center and 2 radii

class TORUS : public OBJECT
{
public:
	POINT*     pCenter;
	FLOAT      fBigRadius, fSmallRadius;

	TORUS(bool bAdd = false);
	TORUS(POINT * center, FLOAT sradius, FLOAT bradius,
		PIGMENT * pigment, TRANSFORM * orient,
		bool bAdd = false);
	~TORUS();

	bool intersect(RAY * pRay, POINT * pIntersect,
		POINT * pNormal);

	void post_process(TRANSFORM * pTrans = NULL);

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Cone : center, radius and height

class CONE : public OBJECT
{
public:
	POINT*     pCenter;
	FLOAT      fRadius, fHeight;

	CONE(bool bAdd = false);
	CONE(POINT * center, FLOAT radius, FLOAT height,
		PIGMENT * pigment, TRANSFORM * orient,
		bool bAdd = false);
	~CONE();

	bool intersect(RAY * pRay, POINT * pIntersect,
		POINT * pNormal);

	void post_process(TRANSFORM * pTrans = NULL);

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Plane : generic equation is fA * x + fB * y + fC * z + fD = 0
// The pNorm holds the normal to the plane, once computed in
// the constructor

class PLANE : public OBJECT
{
public:
	FLOAT      fA, fB, fC, fD;
	POINT*     pNorm;

	PLANE(bool bAdd = false);
	PLANE(FLOAT a, FLOAT b, FLOAT c, FLOAT d,
		PIGMENT * pigment, TRANSFORM * orient,
		bool bAdd = false);
	~PLANE();

	bool intersect(RAY * pRay, POINT * pIntersect,
		POINT * pNormal);

	void post_process(TRANSFORM * pTrans = NULL);

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Polygon object : number of vertices and an array of points,
// dynamically allocated once the number of vertices is known.
// Additionally, it contains a plane object (the plane in which
// the polygon lies), an array of 2D points (the plane on which
// the polygon is projected in order to determine whether the
// intersection point with a given ray lies inside the polygon or
// not) and a constant which identifies which of the 3 ortographic
// planes is the polygon projected onto.

class POLYGON : public OBJECT
{
public:
	int        dNumVertices;
	POINT**    ppPointArray;

	POINT*     pCenter;      // bounding circle optimization
	FLOAT      fRadius;

	// helpers for intersection computations
	PLANE*     pPlane;
	POINT2D*   pCopyArray;
	int        dCollapse;                      // can be dX, dY, dZ

	POLYGON(bool bAdd = false);
	POLYGON(int numVert, bool bAdd = false);
	~POLYGON();

	// intersect a ray from the intersection point with a given
	// edge segment; returns 0 or 1 based on whether the ray
	// intersects the segment or not. The ray starts at
	// pSemi1 and goes ad inifitum through pSemi2.

	int intersect_edge(POINT2D * pSemi1, POINT2D * pSemi2,
		POINT2D * pSeg1, POINT2D * pSeg2);

	bool intersect(RAY * pRay, POINT * pIntersect,
		POINT * pNormal);

	// compute the normal to the polygon
	void compute_normal();
	void post_process(TRANSFORM * pTrans = NULL);

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// =============================================================== 
// Light objects : all linked together on the pList linked list.
// All the lights are point light sources.
// Described by 
//       fIntensity : the intensity of the light source
//       pSource    : the location of the light
//       pRgb       : the RGB color of the light

class LIGHT
{
public:
	static    LLIST* pList;

	FLOAT     fIntensity;
	POINT *   pSource;
	RGB   *   pRgb;

	LIGHT();
	LIGHT(POINT * source, FLOAT intens, RGB * rgb);
	~LIGHT();

	void post_process(TRANSFORM * pTrans = NULL);

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};

// ---------------------------------------------------------------
// Program status class : describes the status of the world and
// other generic parameters.

class WORLD
{
public:
	int      dHeight;                // height of output file
	int      dWidth;                 // width of output file
	int      dSuperSample;           // supersample factor
	int      dDepth;                 // recursion depth for ray tracing

	time_t   timeStart, timeEnd;     // time start and end for ray tracing
	int      dIntersect;             // total number of intersections computed

	POINT    *pEye, *pCoi;           // Eye and Center of Interest
	TRANSFORM * pViewTransform;      // Associated view transformation. This one
									 // is applied after all objects are parsed
									 // in the 'post_process' methods

	PIGMENT* pBackground;            // Background color
	FLOAT    fAmbient;               // Ambient light intensity

	FILEOUT* pOutputFile;            // Output file object

	WORLD();
	~WORLD();

	void post_process();             // Post process (post parsing) initializations
	void print_status();             // Display world status before rendering 
	void print_statistics();         // Display end of rendering statistics

#ifdef __DEBUG
	void print();
#endif  // __DEBUG

};


#endif  // __CLASSES_HH


