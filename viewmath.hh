// ===============================================================
/*
   Ray tracer project

   Razvan Surdulescu
   Summer 1996

   viewmath.hh : 

*/
// =============================================================== 

#ifndef __VIEWMATH_HH
#define __VIEWMATH_HH

TRANSFORM* view_transform(POINT* eye, POINT* coi);
void  move_to_yz(TRANSFORM * pTrans, FLOAT fCos, FLOAT fSin);
void  move_to_z(TRANSFORM * pTrans, FLOAT fCos, FLOAT fSin);

#endif /* __TRANSFORM_H */
